import os
import torch
import cv2
import numpy as np
import torch.backends.cudnn as cudnn
from torch.utils.data import Dataset,DataLoader
import random
from albumentations.pytorch import ToTensorV2
import os
import torch
torch.cuda.empty_cache()
os.environ["PL_TORCH_DISTRIBUTED_BACKEND"] = "gloo"
# def preprocess_mask(mask):
#     mask = mask.astype(np.float32)
#     mask[mask == 255.0] = 1.0
#     mask[mask == 0.0]= 0.0
#     return mask
#
# class SearchDataset(Dataset):
#     def __init__(self,
#                  images_directory,
#                  masks_directory,
#                  transform=None,
#                  seed = 42,
#     ):
#         self.images_directory = images_directory
#         self.masks_directory = masks_directory
#         self.seed = seed
#         self.transform = transform
#         images_filenames = list(sorted(os.listdir(images_directory)))
#         correct_images_filenames = [i for i in images_filenames if
#                                     cv2.imread(os.path.join(images_directory, i)) is not None]
#         random.seed(self.seed)
#         random.shuffle(correct_images_filenames)
#
#         self.images_filenames = correct_images_filenames
#
#     def __len__(self):
#         return len(self.images_filenames)
#
#     def __getitem__(self, idx):
#         image_filename = self.images_filenames[idx]
#         image = cv2.imread(os.path.join(self.images_directory, image_filename))
#         image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
#         mask = cv2.imread(
#             os.path.join(self.masks_directory, image_filename.replace(".jpg", ".png")), cv2.IMREAD_UNCHANGED,
#         )
#         mask = preprocess_mask(mask)
#         if self.transform is not None:
#             transformed = self.transform(image=image, mask=mask)
#             image = transformed["image"]
#             mask = transformed["mask"]
#             try:
#                 mask = torch.unsqueeze(mask, 0)
#             except:
#                 pass
#         return image, mask
#
def preprocess_mask(mask):

    #Some values are not 0 or 255 so map them accordingly before
    mask=(mask>0).astype(np.uint8)*255
    mask = mask.astype(np.float32)
    mask[mask == 255.0] = 1.0
    mask[mask == 0.0]= 0.0
    return mask

class SearchDataset(Dataset):
    """Construct Torch dataset
    Args:
        data_path [str]:      (callbale): path_to raw data
        split [str]:          train or test split
        test_split [float]:   ratio of split between test and train
        step_factor [float]:  step_factor<1 -> not using whole dataset only percentage of if
                              step_factor>1 -> reusing same datasample in one epoch but differently augmented
        transform [str]:      Augmentation policy from albumentations
        external  [str]:      To use external data not provided by competition: deepglobe,massachussets,deepmassa
        seed [int]:           make it reproduciple

    Return:
        transform: albumentations.Compose

    """
    def __init__(self,
                 data_path,
                 split="train",
                 test_split=0,
                 preprocess=None,
                 step_factor=None,
                 transform=None,
                 external="deepglobe",
                 seed = 42,
    ):
        self.images_directory = data_path+"/raw/training/images/"
        self.masks_directory = data_path+"/raw/training/groundtruth/"
        self.external = external
        self.test_split = test_split
        self.seed = seed
        self.transform = transform
        self.preprocess = preprocess
        #Get filenames from main raw directory
        images_filenames = list(sorted(os.listdir(self.images_directory)))
        correct_images_filenames = [i for i in images_filenames if
                                    cv2.imread(os.path.join(self.images_directory, i)) is not None]
        random.seed(self.seed)
        random.shuffle(correct_images_filenames)
        train_images_filenames = correct_images_filenames[
                                 0:np.int32((1 - self.test_split) * len(correct_images_filenames))]
        val_images_filenames = correct_images_filenames[
                               np.int32((1 - self.test_split) * len(correct_images_filenames)):-1]


        if split =="train":
            images_filenames=train_images_filenames
            if step_factor:
                if step_factor>1:
                    temp_filenames = list(images_filenames)
                    for i in range(int(step_factor)):
                        for element in temp_filenames:
                            images_filenames.append(element)
                else:
                    images_filenames = train_images_filenames[0:int(len(train_images_filenames)*step_factor)]
        else:
            images_filenames=val_images_filenames


        #External DATA
        if self.external=="deepglobe" or self.external=="deepmassa":
            self.images_directory_external = data_path + "/external/deepglobe/processed/images/"
            self.masks_directory_external = data_path + "/external/deepglobe/processed/groundtruth/"
            images_filenames_external = list(sorted(os.listdir(self.images_directory_external)))
            correct_images_filenames_external = [i for i in images_filenames_external if
                                        cv2.imread(os.path.join(self.images_directory_external, i)) is not None]
            # correct_images_filenames_external=correct_images_filenames_external[0:add]
            random.seed(self.seed)
            random.shuffle(correct_images_filenames_external)
            train_images_filenames.extend(correct_images_filenames_external)
            random.shuffle(train_images_filenames)
        if self.external=="massachusetts" or self.external=="deepmassa":
            self.images_directory_external = data_path + "/external/massachusetts/processed/images/"
            self.masks_directory_external = data_path + "/external/massachusetts/processed/groundtruth/"
            images_filenames_external = list(sorted(os.listdir(self.images_directory_external)))
            correct_images_filenames_external = [i for i in images_filenames_external if
                                        cv2.imread(os.path.join(self.images_directory_external, i)) is not None]
            # correct_images_filenames_external=correct_images_filenames_external[0:add]
            random.seed(self.seed)
            random.shuffle(correct_images_filenames_external)
            train_images_filenames.extend(correct_images_filenames_external)
            random.shuffle(train_images_filenames)


        self.images_filenames = images_filenames

    def __len__(self):
        return len(self.images_filenames)

    def __getitem__(self, idx):
        image_filename = self.images_filenames[idx]
        if os.path.exists(os.path.join(self.images_directory, image_filename)):
            image = cv2.imread(os.path.join(self.images_directory, image_filename))
            mask = cv2.imread(
                os.path.join(self.masks_directory, image_filename), cv2.IMREAD_UNCHANGED,
            )
        else:
            image = cv2.imread(os.path.join(self.images_directory_external, image_filename))
            mask = cv2.imread(
                os.path.join(self.masks_directory_external, image_filename), cv2.IMREAD_UNCHANGED,
            )

        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        mask = preprocess_mask(mask)
        if self.transform is not None:
            transformed = self.transform(image=image, mask=mask)
            image = transformed["image"]
            mask = transformed["mask"]
            try:
                mask = torch.unsqueeze(mask, 0)
            except:
                pass
        return image, mask
