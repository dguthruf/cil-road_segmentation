# Noise Is All You Need

This is the winning codebase for the [***Kaggle cil-road-segmentation competition at ETHZ.***](https://www.kaggle.com/competitions/cil-road-segmentation-2022/leaderboard)
It is the [**SMP-Based**](https://github.com/qubvel/segmentation_models.pytorch) implementation, 
supplementing the project report [*Noise Is All You Need*](reports/CIL_Report__Noise_Is_All_You_Need.pdf)


![Model Results](reports/figures/results.png)
Samples of predicted segmentation masks of all our models: standalone U-Net and pretrained UNet++,
continuous diffusion model, discrete diffusion model, and discrete diffusion model with pretrained UNet++
architecture.

## Installation

Clone this repository. Then run:

```
pip install -e .
```
and
```
pip install -r requirements.txt
```

## ToDo
1. Create your [.env](.env) file, with your env-variables such as KAGGLE_KEY,KAGGLE_USERNAME, WANDB_API_KEY and WANDB_USERNAME
2. Consider the [ReadmeData](data/README.md) on how to get and use the different datasets.

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   │   ├──            <- EPFL Dataset
    │   │   ├──            <- Roadtracer Dataset
    │   │   └──            <- DeepGlobe Dataset
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── experiments        <- Try out different stuff
    │   ├── autoalbument   <- Autoalbument seraches fitting Augmentation policies by using a GAN network
    │                               where the discriminator tries to differentiate between augmented and not
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data                                <- Scripts to download or generate data
    │   │   └── make_deepglobe_dataset.py       <- Deepglobe road dataset sliced and filtered such that it can be
    │   │                                                levearged on existing dataset
    │   │   └── make_massachussets_dataset.py   <- Massachussets road dataset sliced and filtered such that it can be
    │   │                                                levearged on existing dataset
    │   │   └── make_roadtracer_dataset.py      <- Roadtracer road dataset sliced and filtered such that it can be
    │   │                                                levearged on existing dataset
    │   │
    │   │
    │   ├── models                              <- Define your own Networks here
    │   │
    │   ├── utils                               <- All relevant Scripts are here
    │   │   └── augmentations.py                <- Different augmentations policies that can be called via their name 
    │   │   └── data_modules.py                 <- Data loading classes for raw data
    │   │   └── logger.py                       <- Some logging funciton that facilitate loggin in Wandb
    │   │   └── metrics.py                      <- Defined CLDice loss and Score can be called via this file
    │   │   └── train.py                        <- Main running File either used for tuning or for specif configuration
    │   │   └── train_modules.py                <- Customaizable Train Epoch and Val Epoch classes
    │   │   └── inference.py                    <- Creates Prediction folder of test test and submission_dummy.csv file
    │   │   └── sweep.py                        <- Used to make hyperparameter sweeps via W&B in combination sweep.yaml files
    │   │   └── sweep-grid-hyperband.yaml       <-config file for grid hyperparmeter tuning with wandb
    │   │
    │   └── visualization                       <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io
--------


## Training

Most models (encoder/decoder architectures) are from the [pytorch-segmentation library](https://smp.readthedocs.io/en/latest/).
This codebase is mainly based on it and allows to easily use different encoder/decoder configurations.

To train your model configuration, run
```
python src/utils/train.py --config=custom.yaml
```

To retrain the best model (UnetPlusPlus+efficientnet-b5), run
```
python src/utils/train.py --config=best.yaml
```

To retrain the baseline model (Unet), run
```
python src/utils/train.py --config=baseline.yaml
```

### Notes Training

1. To see the config files and the meaning of the different parameters, navigate to the [config_dir](wandb/config) and adjust the
custom.yaml file as you wish.
2. The training script logs everything in wandb and saves it to [wandb_run_dir](wandb/outputs/wandb), navigating to the 
*./files* provides
summaries, model.pt as dict model_full.pt, config file etc...
3. If your run crashes, use the generated "id" which can be found within the *./wandb_run_dir/run/files* directiory
or in the wandb overview section. Then run 

```
python src/utils/train.py --id=id
```
to restart where you left off.

4. If you run
```
python src/utils/train.py --id=id --config=overwrite.yaml
```
you overwrite your custom.yaml file with the overwrite.yaml file and continue training. (Dont change obvious parameters
like model architecture or optimizer or loss, but be free to change train data, learning rate)

## Inference
The infere your model run:

```
python src/utils/inference.py --threshold= --model==
```

To infere your latest model, run
```
python src/utils/inference.py
```
this prompt navigates to the [wandb_run_dir](wandb/outputs/wandb) folder and selects the latest run

To infere the best model, run
```
python src/utils/inference.py --model=best
```

To infere the baseline model, run
```
python src/utils/inference.py --model=baseline
```
To infere any model in a folder structure */run/files/*, run
```
python src/utils/inference.py --model_path=.../run/
```
with the absolute path to *C:/User/.../run/*

### Notes Inference
1. The threshold value is relevant for the cut of the mask to assign 0 or 1. Sadly the automatic
calculation of the optimal threshold does not result with the best submission file... Therefore one
can set different thresholds and submit each version and iterative find the best threshold like that.
This is far from optimal but the approach of running the inference multiple times with different
thresholds and picking the run with the highest metric score was for some reason faulty...
2. This prompt creates a submission.csv file within the */run/files/* folder ready for upload, additionally 
mask predictions and their patched version are added

## Hyperparameter tuning
The hyperparameter tuning is performed with the Wandb Sweeps package and also logged on wandb.

To configure your sweep (hyperparameter configuration) navigate to [sweep_config_file](src/utils/sweep-grid-hyperband.yaml)
Navigate [to](src/utils/)  before you run the sweep

Then run
```
wandb sweep sweep-grid-hyperband.yaml
```
this will start an agent which provides a prompt that can be run in the console to start the sweep

### Notes HP-Tuning
1. To use bayesian or hyperband sweeps consider wandb documentation and adjust the sweep.yaml file.

## Autoalbument

Autoalbument searches fitting Augmentation policies by using a GAN network where the discriminator 
tries to differentiate between augmented and not.

Configure the search.yaml file. Then run
```
autoalbument-search --config-dir ./experiments/autoalbument-search-road/
```

### Notes Autoalbument

1. Sadly no performance improving augmentation policy was found, but it could be further explored.


## Additional Notes

1. All wandb logs are availabe at [dguthruf_wandb](https://wandb.ai/dguthruf/Road?workspace=user-dguthruf)
2. All sweeps are availabe at [dguthruf_sweeps](https://wandb.ai/dguthruf/Road/sweeps?workspace=user-dguthruf)
3. Also consider artifacts of the main runs to see how mask predictiosn evolve e.g.
[best_run](https://wandb.ai/dguthruf/Road/runs/6bkvoihk?workspace=user-dguthruf)



