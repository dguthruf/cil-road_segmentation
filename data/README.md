## Data

Apart from the original training dataset, we provide several more options. Such as the 
[DeepGlobe Dataset](https://www.kaggle.com/datasets/balraj98/deepglobe-road-extraction-dataset), 
the [EPFL Dataset](https://github.com/arthurbabey/road66),
the [Roadtracer Dataset](https://github.com/mitroadmaps/roadtracer) and 
the [Massachuessets Dataset](https://www.kaggle.com/datasets/balraj98/massachusetts-buildings-dataset).

### How do I get the data?
There are two ways to get the data, first one is easier.
The GitRepo [DataFolder](data/) only contains the orginal and the epfl data, since they are both relatively small.


#### 1.
All data is available for download at [Polybox](https://polybox.ethz.ch/index.php/s/USLJotE9cgtZPMr).
Unzip the data and setup the data as shown below.

#### 2.
The [SrcDataFolder](src/data/) contains several make_datasets.py files, which download, process and save the images ready
for usage. The [make_deepglobe_dataset.py](src/data/make_deepglobe_dataset.py) and the [make_massachusetts_dataset.py](src/data/make_massachusetts_dataset.py)
should be usable out of the box if you have a KAGGLE_KEY and KAGGLE_USER_NAME defined in [.env](.env). 
The [make_roadtracer_dataset.py](src/data/make_roadtracer_dataset.py) is a bit more complicated, since the dataload is much larger (100 Gb)
and one must use the the [Github](https://github.com/mitroadmaps/roadtracer) instructions, to get the inital images and masks.
If downloaded sucessfully you can use the [make_roadtracer_dataset.py](src/data/make_roadtracer_dataset.py) to filter and
slice the images accordingly.


#### Final Data-Folder Structure
    data
    ├───raw                             <-original dataset, provided by ethz
    │   ├───test
    │   │   └───images                 
    │   │       │   satimage_144.png
    │   │       │   satimage_145.png
    │   │       │   ...
    │   └───training
    │           ├───groundtruth
    │           │   │   satimage_0.png
    │           │   │   satimage_1.png
    │           │   │   ...
    │           └───images
    │               │   satimage_0.png
    │               │   satimage_1.png
    │               │   ...
    │──external
    │   ├───deepglobe
    │   │   └───processed
    │   │       ├───groundtruth
    │   │       │   │   deepsat_1.png
    │   │       │   │   deepsat_2.png
    │   │       │   │   ...
    │   │       └───images
    │   │           │   deepsat_1.png
    │   │           │   deepsat_2.png
    │   │           │   ...
    │   ├───epfl
    │   │   └───processed
    │   │       ├───groundtruth
    │   │       └───images
    │   ├───roadtracing
    │   │   └───processed
    │   │       ├───groundtruth
    │   │       └───images
    │   ├───massachuesetts
    │   │   └───processed
    │   │       ├───groundtruth
    │   │       └───images

### Notes Data

1. The additional datasets do not correspond to the exact [meter/pixel] ratio of the original dataset. Using the average
road width (both lines) of about 7m one can estimate the [meter/pixel] ratio for the datasets and adjust them accordingly,
to reduce the deviation from the original [meter/pixel] ratio as much as possible.
2. The Massachuessets and Roadtracer dataset both have same mask road width (rather thin) for small country roads and 
highways which is in contradiction to the original dataset, where the mask road width of a street is different depending
on the street type. We tried to reduce this bias to thin mask roads by implementing dilation and erosion augmentations
without much success. Nevertheless even with this thin road bias we achieve a better performance than without using the
additional datasets
3. We did not correct for Stain-Normalization of the external datasets (which could further improve the performance).
