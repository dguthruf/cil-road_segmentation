import torch.nn as nn
from torchvision.models.swin_transformer import SwinTransformer, Swin_B_Weights, Swin_S_Weights, Swin_T_Weights, \
    SwinTransformerBlock
from pretrainedmodels.models.torchvision_models import pretrained_settings
from segmentation_models_pytorch.encoders._base import EncoderMixin
import torch.nn.functional as F

from copy import deepcopy


class SwinTransformerEncoder(SwinTransformer, EncoderMixin):
    def __init__(self, out_channels, depth=4, **kwargs):
        super().__init__(**kwargs)
        self._depth = depth
        self._out_channels = out_channels
        self._in_channels = 3

        del self.head

    def get_stages(self):
        return [
            nn.Identity(),
            nn.Sequential(self.features[0], self.features[1]),
            nn.Sequential(self.features[2], self.features[3]),
            nn.Sequential(self.features[4], self.features[5]),
            nn.Sequential(self.features[6], self.features[7]),
        ]

    # def forward(self, x):
    #     stages = self.get_stages()
    #
    #     features = []
    #     out_channels=[]
    #     for i in range(self._depth + 1):
    #         x = stages[i](x)
    #         print(x.size())
    #         out_channels.append(x.size()[3])
    #         features.append(x)
    #     out_channels[0]=3
    #     self._out_channels=tuple(out_channels)
    #     print(self._out_channels)
    #     return features
    def forward(self, x):
        stages = self.get_stages()

        features = []
        for i in range(self._depth + 1):
            x = stages[i](x)
            if i>0:
                features.append(F.interpolate(x.permute(0,3,1,2),scale_factor=2,mode="nearest"))
                # features.append(x.permute(0,3,1,2))
            else:
                features.append(x)
        return features
    def load_state_dict(self, state_dict, **kwargs):
        state_dict.pop("head.bias", None)
        state_dict.pop("head.weight", None)
        super().load_state_dict(state_dict, **kwargs)


new_settings = {
    "swin_t": {
        "imagenet": "https://download.pytorch.org/models/swin_t-704ceda3.pth",  # noqa
    },
    "swin_s": {
        "imagenet": "https://download.pytorch.org/models/swin_s-5e29d889.pth",  # noqa
    },
    "swin_b": {
        "imagenet": "https://download.pytorch.org/models/swin_b-68c6b09e.pth",  # noqa
    },
}

pretrained_settings = deepcopy(pretrained_settings)
for model_name, sources in new_settings.items():
    if model_name not in pretrained_settings:
        pretrained_settings[model_name] = {}

    for source_name, source_url in sources.items():
        pretrained_settings[model_name][source_name] = {
            "url": source_url,
            "input_size": [3, 224, 224],
            "input_range": [0, 1],
            "mean": [0.485, 0.456, 0.406],
            "std": [0.229, 0.224, 0.225],
            "num_classes": 1000,
        }

swintransformer_encoders = {
    "swin_t": {
        "encoder": SwinTransformerEncoder,
        "pretrained_settings": pretrained_settings["swin_t"],
        "params": {
            "out_channels": (3, 96, 192, 384, 768, 768),
            "patch_size": [4, 4],
            "embed_dim": 96,
            "depths": [2, 2, 6, 2],
            "num_heads": [3, 6, 12, 24],
            "window_size": [7, 7],
            "stochastic_depth_prob": 0.2,
        },
    },
    "swin_s": {
        "encoder": SwinTransformerEncoder,
        "pretrained_settings": pretrained_settings["swin_s"],
        "params": {
            "out_channels":(3, 96, 192, 384, 768, 768),
            "patch_size": [4, 4],
            "embed_dim": 96,
            "depths": [2, 2, 18, 2],
            "num_heads": [3, 6, 12, 24],
            "window_size": [7, 7],
            "stochastic_depth_prob": 0.3,
        },
    },
    "swin_b": {
        "encoder": SwinTransformerEncoder,
        "pretrained_settings": pretrained_settings["swin_b"],
        "params": {
            "out_channels": (3, 128, 256, 512, 1024, 2048),
            "patch_size": [4, 4],
            "embed_dim": 128,
            "depths": [2, 2, 18, 2],
            "num_heads": [4, 8, 16, 32],
            "window_size": [7, 7],
            "stochastic_depth_prob": 0.5,
        },
    },
}