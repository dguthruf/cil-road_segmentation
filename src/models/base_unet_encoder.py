import torch
from segmentation_models_pytorch.encoders._base import EncoderMixin
import segmentation_models_pytorch as smp
from typing import List
import torch.nn as nn
import torch.nn.functional as F

class DoubleConv(nn.Module):
    """(convolution => [BN] => ReLU) * 2"""

    def __init__(self, in_channels, out_channels, mid_channels=None):
        super().__init__()
        if not mid_channels:
            mid_channels = out_channels
        self.double_conv = nn.Sequential(
            nn.Conv2d(in_channels, mid_channels, kernel_size=3, padding=1, bias=False),
            nn.BatchNorm2d(mid_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid_channels, out_channels, kernel_size=3, padding=1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True).to(device = 'cuda:0')
        )

    def forward(self, x):
        return self.double_conv(x).to(device = 'cuda:0')

class Down(nn.Module):
    """Downscaling with maxpool then double conv"""

    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool2d(2),
            DoubleConv(in_channels, out_channels)
        )

    def forward(self, x):
        return self.maxpool_conv(x).to(device = 'cuda:0')

class BaseUnet(torch.nn.Module, EncoderMixin):

    def __init__(self, bilinear=False,**kwargs):
        super().__init__()

        # A number of channels for each encoder feature tensor, list of integers
        self._out_channels: List[int] = [3, 64, 128, 256, 512, 1024]
        # A number of stages in decoder (in other words number of downsampling operations), integer
        # use in in forward pass to reduce number of returning features
        self._depth: int = 4

        # Default number of input channels in first Conv2d layer for encoder (usually 3)
        self._in_channels: int = 3

    def get_stages(self):
        return [
                nn.Identity(),
                nn.Sequential(DoubleConv(self._out_channels[0],self._out_channels[1]).to(device = 'cuda:0'),Down(self._out_channels[1], self._out_channels[2]).to(device = 'cuda:0')).to(device = 'cuda:0'),
                Down(self._out_channels[2], self._out_channels[3]).to(device = 'cuda:0'),
                Down(self._out_channels[3], self._out_channels[4]).to(device = 'cuda:0'),
                Down(self._out_channels[4], self._out_channels[5]//2).to(device = 'cuda:0'),
        ]

    def forward(self, x: torch.Tensor) -> List[torch.Tensor]:
        stages = self.get_stages()

        features = []
        for i in range(self._depth + 1):
            x = stages[i](x)
            features.append(x)
        return features