#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : Daniel Guthruf
# Created Date: 27.06.2022
# =============================================================================
"""
Filename:           make_deepglobe_dataset.py
Description:        Creates dataset from deepglobe dataset
Main Libs:          kaggle,image_slicer
Long Description:   Use kaggle deepglobe_dataset to with 1024x1024pix, which is
                    sliced into 4 new images resized to 400x400 and filtered
                    such that only high road frequency images are kept. Thresh
                    hold is set manually which to keep which to throw away
                    Steps are:
                    1.  Data download via kaggle api (you need kaggle user and
                        api key set in .env file)
                    2.  Read data, filter by np.mean(mask)<threshold1
                    3.  Slice into 2x2 matrix of images (4 subimages),
                        save to data/src
                    4.  Filter again by np.mean(mask)<threshold2
                        save to data/processed
State:              dev.
"""
import argparse
import os
# =============================================================================
# Imports
# =============================================================================
import shutil
import zipfile
from pathlib import Path

import cv2
import dotenv
import numpy as np
from image_slicer import slice
from kaggle.api.kaggle_api_extended import KaggleApi
from PIL import Image
from tqdm import tqdm

# =============================================================================
# Paths
# =============================================================================
PROJECT_DIR=Path(__file__).parent.parent.parent
DATA_DIR = os.path.join(PROJECT_DIR,"data/external/deepglobe/")
DATA_RAW = os.path.join(DATA_DIR,"raw/")
DATA_RAW_TRAIN = os.path.join(DATA_RAW, 'train/')
SLICED_MASK_DIR= os.path.join(DATA_DIR,"interm/groundtruth/")
SLICED_IMAGE_DIR= os.path.join(DATA_DIR,"interm/images/")
PROCESSED_MASK_DIR= os.path.join(DATA_DIR,"processed/groundtruth/")
PROCESSED_IMAGE_DIR= os.path.join(DATA_DIR,"processed/images/")
RAW_DATA= os.path.join(PROJECT_DIR,"data/raw/training/groundtruth/")


# =============================================================================
# Load enviornment variables
# =============================================================================
dotenv_path = os.path.join(PROJECT_DIR, '.env')
dotenv.load_dotenv(dotenv_path)
api = KaggleApi()
api.authenticate()


parser = argparse.ArgumentParser()
parser.add_argument('-im','--intermediate',type=bool,default=False,action=argparse.BooleanOptionalAction,help="Redo Finding of HF-Images and Slicing?")
args = parser.parse_args()


def main():
    # =============================================================================
    # Step 1: Download and Zip
    # =============================================================================
    if not os.path.exists(os.path.join(DATA_RAW, 'deepglobe-road-extraction-dataset.zip')):
        Path(DATA_RAW).mkdir(parents=True, exist_ok=True)
        api.dataset_download_files('balraj98/deepglobe-road-extraction-dataset',path=DATA_RAW)
        z = zipfile.ZipFile(file=os.path.join(DATA_RAW, 'deepglobe-road-extraction-dataset.zip'))
        z.extractall(path=DATA_RAW)
    # =============================================================================
    # Step 2: Filter low frequency road images out and images with missing parts
    # =============================================================================
    if args.intermediate:
        filenames = list(sorted(os.listdir(DATA_RAW_TRAIN)))
        mask_filenames = filenames[::2]
        image_filenames = filenames[1::2]
        hf_images=[]
        hf_masks = []
        print(f"Total Raw Images: {len(mask_filenames)}\n")

        for mask_fn,image_fn in tqdm(zip(mask_filenames,image_filenames),total=len(mask_filenames)):
            mask = cv2.imread(os.path.join(DATA_RAW_TRAIN, mask_fn), cv2.IMREAD_UNCHANGED)
            if np.mean(mask)>15:
                hf_masks.append(mask_fn)
                hf_images.append(image_fn)
        try:
            shutil.rmtree(SLICED_IMAGE_DIR)
            shutil.rmtree(SLICED_MASK_DIR)
        except:
            pass
        print(f"\nTotal HF Images: {len(hf_images)}\n")
        Path(SLICED_MASK_DIR).mkdir(parents=True, exist_ok=True)
        Path(SLICED_IMAGE_DIR).mkdir(parents=True, exist_ok=True)
        # =============================================================================
        # Step 3: Slicing images
        # =============================================================================
        number=9
        i = 0
        for mask_fn,image_fn in tqdm(zip(hf_masks,hf_images),total=len(hf_masks)):
            images=slice(filename=DATA_RAW_TRAIN+image_fn,
                         number_tiles=number,
                         save=False)
            masks=slice(filename=DATA_RAW_TRAIN+mask_fn,
                        number_tiles=number,
                        save=False)
            for im,ma in zip(images,masks):
                im.save(filename=os.path.join(SLICED_IMAGE_DIR+f"image_{i}.png"))
                ma.save(filename=os.path.join(SLICED_MASK_DIR+f"mask_{i}.png"))
                i+=1
    try:
        shutil.rmtree(PROCESSED_IMAGE_DIR)
        shutil.rmtree(PROCESSED_MASK_DIR)
    except:
        pass
    Path(PROCESSED_IMAGE_DIR).mkdir(parents=True, exist_ok=True)
    Path(PROCESSED_MASK_DIR).mkdir(parents=True, exist_ok=True)
    sliced_mask_filenames = list(sorted(os.listdir(SLICED_MASK_DIR)))
    sliced_image_filenames=list(sorted(os.listdir(SLICED_IMAGE_DIR)))
    hff_images=[]
    hff_masks = []
    i=0
    # =============================================================================
    # Step 4: Filter low frequency road images and save
    # =============================================================================
    for mask_fn,image_fn in tqdm(zip(sliced_mask_filenames,sliced_image_filenames),total=len(sliced_mask_filenames)):
        mask = cv2.imread(os.path.join(SLICED_MASK_DIR, mask_fn), cv2.IMREAD_UNCHANGED)
        image = cv2.imread(os.path.join(SLICED_IMAGE_DIR, image_fn), cv2.IMREAD_UNCHANGED)
        if np.mean(mask)>20 and np.mean(image)>110:
            mask = Image.fromarray(mask)
            mask = mask.convert("L")
            mask = mask.resize((400,400))
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            image = Image.fromarray(image)
            image = image.resize((400,400))
            mask.save(PROCESSED_MASK_DIR + "deepsat_"+str(i)+".png")
            image.save(PROCESSED_IMAGE_DIR + "deepsat_"+str(i)+".png")

            hff_masks.append(mask_fn)
            hff_images.append(image_fn)
        i+=1


    print(f"Total Sliced Images: {len(sliced_mask_filenames)}\n"
          f"Total Sliced HF Images:{len(hff_masks)}")


if __name__ == '__main__':
    main()