#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : Daniel Guthruf
# Created Date: 27.06.2022
# =============================================================================
"""
Filename:           make_massachusetts_dataset.py
Description:        Creates dataset from massachusetts dataset
Main Libs:          kaggle,image_slicer
Long Description:   Use kaggle massachusetts_dataset to with 1500x1500pix, which is
                    sliced into 25 new images resized to 400x400 and filtered
                    such that only high road frequency images are kept. Thresh
                    hold is set manually which to keep which to throw away
                    Steps are:
                    1.  Data download via kaggle api (you need kaggle user and
                        api key set in .env file)
                    2.  Read data, filter by np.mean(mask)<threshold1 and remove
                        images with mssing parts
                    3.  Slice into 5x5 matrix of images (25 sub images),
                        save to data/src
                    4.  Filter again by np.mean(mask)<threshold2
                        save to data/processed
State:              dev.
"""
import argparse
# =============================================================================
# Imports
# =============================================================================
import os
import shutil
import zipfile
from pathlib import Path

import cv2
import dotenv
import numpy as np
from image_slicer import slice
from kaggle.api.kaggle_api_extended import KaggleApi
from PIL import Image
from tqdm import tqdm

# =============================================================================
# Paths
# =============================================================================
PROJECT_DIR=Path(__file__).parent.parent.parent
DATA_DIR = os.path.join(PROJECT_DIR,"data/external/massachusetts/")
DATA_RAW = os.path.join(DATA_DIR,"raw/")
IMAGE_DIR = os.path.join(DATA_RAW, 'tiff/train/')
MASK_DIR = os.path.join(DATA_RAW, 'tiff/train_labels/')
SLICED_MASK_DIR= os.path.join(DATA_DIR,"interm/groundtruth/")
SLICED_IMAGE_DIR= os.path.join(DATA_DIR,"interm/images/")
PROCESSED_MASK_DIR= os.path.join(DATA_DIR,"processed/groundtruth/")
PROCESSED_IMAGE_DIR= os.path.join(DATA_DIR,"processed/images/")
RAW_DATA= os.path.join(PROJECT_DIR,"data/raw/training/groundtruth/")

# =============================================================================
# Load enviornment variables
# =============================================================================
dotenv_path = os.path.join(PROJECT_DIR, '.env')
dotenv.load_dotenv(dotenv_path)
api = KaggleApi()
api.authenticate()


parser = argparse.ArgumentParser()
parser.add_argument('-im','--intermediate',type=bool,default=True,action=argparse.BooleanOptionalAction,help="Redo Finding of HF-Images and Slicing?")
args = parser.parse_args()
def main():

    # =============================================================================
    # Step 1: Download and Zip
    # =============================================================================
    if not os.path.exists(os.path.join(DATA_RAW, 'massachusetts-roads-dataset.zip')):
        Path(DATA_RAW).mkdir(parents=True, exist_ok=True)
        api.dataset_download_files('balraj98/massachusetts-roads-dataset',path=DATA_RAW)
        z = zipfile.ZipFile(file=os.path.join(DATA_RAW, 'massachusetts-roads-dataset.zip'))
        z.extractall(path=DATA_RAW)


    # =============================================================================
    # Step 2: Filter low frequency road images out and images with missing parts
    # =============================================================================
    if args.intermediate:
        # Read all file names from ./data/external/raw
        mask_filenames = list(sorted(os.listdir(MASK_DIR)))
        image_filenames = list(sorted(os.listdir(IMAGE_DIR)))
        hf_images=[]
        hf_masks = []
        print(f"Total Raw Images: {len(mask_filenames)}\n")

        for mask_fn,image_fn in tqdm(zip(mask_filenames,image_filenames),total=len(mask_filenames)):
            mask = cv2.imread(os.path.join(MASK_DIR, mask_fn), cv2.IMREAD_UNCHANGED)
            image= cv2.imread(os.path.join(IMAGE_DIR, image_fn), cv2.IMREAD_UNCHANGED)
            #All masks<15 have not lot of roads, all images with many white pixels are removed
            if np.mean(mask)>20 and len(image.flatten()[image.flatten()==255])<1000:
                hf_masks.append(mask_fn)
                hf_images.append(image_fn)
        try:
            shutil.rmtree(SLICED_IMAGE_DIR)
            shutil.rmtree(SLICED_MASK_DIR)
        except:
            pass
        print(f"\nTotal HF Images: {len(hf_images)}\n")

        Path(SLICED_MASK_DIR).mkdir(parents=True, exist_ok=True)
        Path(SLICED_IMAGE_DIR).mkdir(parents=True, exist_ok=True)
        # =============================================================================
        # Step 3: Slicing images
        # =============================================================================
        number=25
        i = 0
        for mask_fn,image_fn in tqdm(zip(hf_masks,hf_images),total=len(hf_masks)):
            images=slice(filename=IMAGE_DIR+image_fn,
                         number_tiles=number,
                         save=False)
            masks=slice(filename=MASK_DIR+mask_fn,
                        number_tiles=number,
                        save=False)
            for im,ma in zip(images,masks):
                im.save(filename=os.path.join(SLICED_IMAGE_DIR+f"image_{i}.png"))
                ma.save(filename=os.path.join(SLICED_MASK_DIR+f"mask_{i}.png"))
                i+=1
    try:
        shutil.rmtree(PROCESSED_IMAGE_DIR)
        shutil.rmtree(PROCESSED_MASK_DIR)
    except:
        pass
    Path(PROCESSED_IMAGE_DIR).mkdir(parents=True, exist_ok=True)
    Path(PROCESSED_MASK_DIR).mkdir(parents=True, exist_ok=True)
    sliced_mask_filenames = list(sorted(os.listdir(SLICED_MASK_DIR)))
    sliced_image_filenames=list(sorted(os.listdir(SLICED_IMAGE_DIR)))
    hff_images=[]
    hff_masks = []
    i=0
    # =============================================================================
    # Step 4: Filter low frequency road images and save
    # =============================================================================
    for mask_fn,image_fn in tqdm(zip(sliced_mask_filenames,sliced_image_filenames),total=len(sliced_mask_filenames)):
        mask = cv2.imread(os.path.join(SLICED_MASK_DIR, mask_fn), cv2.IMREAD_UNCHANGED)
        image = cv2.imread(os.path.join(SLICED_IMAGE_DIR, image_fn), cv2.IMREAD_UNCHANGED)
        if np.mean(mask)>20:
            mask = Image.fromarray(mask)
            mask = mask.convert("L")
            mask = mask.resize((400,400))
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            image = Image.fromarray(image)
            image = image.resize((400,400))
            mask.save(PROCESSED_MASK_DIR + "massasat_"+str(i)+".png")
            image.save(PROCESSED_IMAGE_DIR + "massasat_"+str(i)+".png")

            hff_masks.append(mask_fn)
            hff_images.append(image_fn)
        i+=1


    print(f"Total Sliced Images: {len(sliced_mask_filenames)}\n"
          f"Total Sliced HF Images:{len(hff_masks)}")


if __name__ == '__main__':
    main()