# -*- coding: utf-8 -*-
import albumentations as A
import numpy as np
import segmentation_models_pytorch as smp
import torch
import wandb
from matplotlib import pyplot as plt
from torch.utils.data import DataLoader

device = torch.device('cuda:0') if torch.cuda.is_available() else torch.device('cpu')


def wb_mask(bg_img, pred_mask, true_mask):
    return wandb.Image(bg_img, masks={
        "prediction": {"mask_data": pred_mask},
        "ground truth": {"mask_data": true_mask}})


def wandb_predictions(dataset, model):
    images, masks = next(iter(dataset))
    predicted_masks = model.predict(images.to(device)).permute(0, 2, 3, 1).cpu().numpy()
    images = images.permute(0, 2, 3, 1).cpu().numpy()
    masks = masks.permute(0, 2, 3, 1).cpu().numpy().astype(np.uint8)
    mask_list = []
    for i in range(len(images)):
        image = images[i]
        mask = masks[i]
        mask = mask.reshape(mask.shape[0], mask.shape[1])
        predicted_mask = predicted_masks[i]
        predicted_mask = (predicted_mask > 0.5).astype(np.uint8)
        predicted_mask = predicted_mask.reshape(predicted_mask.shape[0], predicted_mask.shape[1])
        # keep a list of composite images
        mask_list.append(wb_mask(image, predicted_mask, mask))
        # log all composite images to W&B
    wandb.log({"predictions": mask_list})


def log_validation_artifacts(test_table, dataset, model, threshold=0.5,number=30):
    images,masks=[],[]
    for image,mask in dataset:
        images.append(image)
        masks.append(mask)
    images = torch.stack(images[0:number])
    masks = torch.stack(masks[0:number])
    predicted_masks = model.predict(images.to(device))
    images = images.permute(0, 2, 3, 1).cpu().numpy()
    tp, fp, fn, tn = smp.metrics.get_stats(output=predicted_masks.cpu().detach(), target=masks.long().cpu().detach(),
                                           mode='binary',
                                           threshold=threshold)
    predicted_masks = (predicted_masks > threshold).type(torch.float32)
    predicted_masks = predicted_masks.permute(0, 2, 3, 1).cpu().numpy()
    masks = masks.permute(0, 2, 3, 1).cpu().numpy().astype(np.uint8)
    accuracies = smp.metrics.accuracy(tp, fp, fn, tn, reduction=None)
    ious = smp.metrics.iou_score(tp, fp, fn, tn, reduction=None)
    f1s = smp.metrics.f1_score(tp, fp, fn, tn, reduction=None)
    # assing ids based on the order of the images
    _id = 0
    for i, tm, pm, s, iou,f1 in zip(images, masks, predicted_masks, accuracies, ious,f1s):
        # add required info to data table:
        # id, image pixels, model's guess, true label, scores for all classes
        tm = tm.reshape(tm.shape[0], tm.shape[1])
        pm = pm.reshape(pm.shape[0], pm.shape[1])
        img_id = str(_id)
        test_table.add_data(img_id, wandb.Image(i), wandb.Image(tm), wandb.Image(pm), s.numpy()[0], iou.numpy()[0],f1.numpy()[0])
        _id += 1


def best_threshold(model, test_dataset, batch_size, threshold=0.5):
    test_loader = DataLoader(
        test_dataset, batch_size=batch_size, shuffle=False, num_workers=0, pin_memory=True,
    )
    model.eval()
    accuracy = []
    f1score = []
    iou = []
    with torch.no_grad():
        for images, true_masks in test_loader:
            true_list = []
            predictions_list = []
            images = images.to(device)
            probabilities = model.predict(images)
            predicted_masks = torch.sigmoid(probabilities).permute(0, 2, 3, 1).cpu().numpy()
            predicted_masks = (predicted_masks > threshold).astype(np.uint8)
            crop = A.Compose([
                A.CenterCrop(height=400, width=400),
            ])
            true_masks = true_masks.permute(0, 2, 3, 1).cpu().numpy()
            for true_mask, mask in zip(true_masks, predicted_masks):
                cropped = crop(image=true_mask, mask=mask)
                true_mask = cropped["image"]
                pred_mask = cropped["mask"]
                true_mask = patch(true_mask)
                pred_mask = patch(pred_mask)
                true_list.append(true_mask)
                predictions_list.append(pred_mask)
            trues = np.asarray(true_list)
            trues = trues.reshape(trues.shape[0], 1, trues.shape[1], trues.shape[2])
            trues = torch.from_numpy(trues)
            preds = np.asarray(predictions_list)
            preds = preds.reshape(preds.shape[0], 1, preds.shape[1], preds.shape[2])
            preds = torch.from_numpy(preds)

            tp, fp, fn, tn = smp.metrics.get_stats(preds / 255, trues // 255, mode="binary", threshold=0.1)
            accuracy.append(smp.metrics.accuracy(tp, fp, fn, tn, reduction="micro"))
            f1score.append(smp.metrics.f1_score(tp, fp, fn, tn, reduction="micro"))
            iou.append(smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro"))

    return np.mean(accuracy), np.mean(f1score), np.mean(iou)


foreground_threshold = 0.25  # percentage of pixels of val 255 required to assign a foreground label to a patch


def patch_to_label(patch):
    patch = patch.astype(np.float64) / 255
    df = np.mean(patch)
    if df > foreground_threshold:
        return 1
    else:
        return 0


def patch(im_arr):
    im_arr = (im_arr.reshape(400, 400) * 255.0).astype(np.uint8)
    patch_size = 16
    mask = np.zeros_like(im_arr)
    for j in range(0, im_arr.shape[1], patch_size):
        for i in range(0, im_arr.shape[0], patch_size):
            patch = im_arr[i:i + patch_size, j:j + patch_size]
            label = patch_to_label(patch)
            mask[i:i + patch_size, j:j + patch_size] = int(label * 255)
    return mask


def log_threshold(best_model, wandb, valid_dataset, resolution=30):
    thresholds = np.linspace(0.01, 0.99, resolution)
    accuracy_list = []
    f1score_list = []
    iou_list = []
    for threshhold in thresholds:
        accuracy, f1score, iou = best_threshold(best_model, valid_dataset, batch_size=12, threshold=threshhold)
        accuracy_list.append(accuracy)
        f1score_list.append(f1score)
        iou_list.append(iou)
    fig = plt.figure(figsize=(18, 12))
    plt.plot(thresholds, accuracy_list, "-b")
    plt.plot(thresholds, f1score_list, "-g")
    plt.plot(thresholds, iou_list, "-r")
    plt.plot(thresholds[np.argmax(accuracy_list)], accuracy_list[np.argmax(accuracy_list)], marker="*", color="r")
    plt.plot(thresholds[np.argmax(f1score_list)], f1score_list[np.argmax(f1score_list)], marker="*", color="r")
    plt.plot(thresholds[np.argmax(iou_list)], iou_list[np.argmax(iou_list)], marker="*", color="b")

    plt.title(
        F"Best Accuracy: {np.round(accuracy_list[np.argmax(accuracy_list)], 5)} at T={np.round(thresholds[np.argmax(accuracy_list)], 5)} \n "
        F"Best F1Score: {np.round(f1score_list[np.argmax(f1score_list)], 5)} at T={np.round(thresholds[np.argmax(f1score_list)], 5)}\n"
        F"Best IouScore: {np.round(iou_list[np.argmax(iou_list)], 5)} at T={np.round(thresholds[np.argmax(iou_list)], 5)}")

    wandb.log({"Threshold": wandb.Image(plt)})
    wandb.config.update({"threshold": thresholds[np.argmax(accuracy_list)]}, allow_val_change=True)


def log_submission_predictions(best_model, wandb, test_dataset):
    test_loader = DataLoader(
        test_dataset, batch_size=12, shuffle=False, num_workers=0, pin_memory=True,
    )
    images_list = []
    predictions_list = []
    with torch.no_grad():
        for images in test_loader:
            images = images.to(device)
            probabilities = best_model.predict(images)
            predicted_masks = torch.sigmoid(probabilities).permute(0, 2, 3, 1).cpu().numpy()
            predicted_masks = (predicted_masks > wandb.config.threshold).astype(np.uint8)
            images = images.permute(0, 2, 3, 1).cpu().numpy()
            crop = A.Compose([
                A.CenterCrop(height=400, width=400),
            ])

            for image, mask in zip(images, predicted_masks):
                cropped = crop(image=image, mask=mask)
                image = cropped["image"]
                mask = cropped["mask"]
                images_list.append(image)
                predictions_list.append(mask)

    test_data_at = wandb.Artifact("Submission" + str(wandb.run.id), type="predictions")
    test_data_at.add_dir(wandb.run.dir)
    columns = ["id", "image", "guess"]
    test_table = wandb.Table(columns=columns)
    # assing ids based on the order of the images
    _id = 0
    for tm, pm in zip(images_list, predictions_list):
        # pm = pm.reshape(pm.shape[1], pm.shape[2])
        img_id = str(_id)
        test_table.add_data(img_id, wandb.Image(tm), wandb.Image(pm))
        _id += 1
    test_data_at.add(test_table, "predictions")
    wandb.run.log_artifact(test_data_at)
    return predictions_list
