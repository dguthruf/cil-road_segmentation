#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : Daniel Guthruf
# Created Date: 27.06.2022
# =============================================================================
"""
Filename:           sweep.py
Description:        Wandb sweep
Main Libs:          Wandb,segmentation_models_pytorch
Long Description:   Choose encoder,decoder,loss,optimizer etc... via
                    console inputs or in yaml file for hyperparatuning.
                    Not only is the basic dataset available, with make_datasets
                    files in src/data you can generate deepglobe and massachues
                    for more data. Everything is logged in wandb.
State:              dev
"""
# =============================================================================
# Imports
# =============================================================================
import warnings

warnings.filterwarnings("ignore", category=FutureWarning)
import torch.backends.cudnn as cudnn

cudnn.benchmark = True
import os
import random
import wandb
import numpy as np
import torch
import argparse
import dotenv
from pathlib import Path
from torch.utils.data import DataLoader
import segmentation_models_pytorch as smp
from src.utils.metrics import get_loss_function
from src.utils.augmentations import get_augmentation
from src.utils.train_modules import TrainEpoch, ValidEpoch
from src.utils.data_modules import RoadDataset, RoadDataTestSet, get_preprocessing
from src.utils.logger import log_threshold


def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2 ** 32
    np.random.seed(worker_seed)
    random.seed(worker_seed)


def seed_all(seed: int) -> None:
    """
    Setting all the relevant seed to make run reproducible
    @param seed:
    """
    if not seed:
        seed = 10

    print("[ Using Seed : ", seed, " ]")

    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.cuda.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)
    # torch.use_deterministic_algorithms(True)
    g = torch.Generator()
    g.manual_seed(0)
    # torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False


def main():
    segmentation_model = getattr(smp, config.segmentation_model)
    if config.encoder_weights=="imagenet":
        preprocessing_fn = smp.encoders.get_preprocessing_fn(config.encoder, config.encoder_weights)
        preprocessing_fn = get_preprocessing(preprocessing_fn)
        encoder_weights = config.encoder_weights
    else:
        preprocessing_fn = None
        encoder_weights=None

    decoder_channels = [config.node1,config.node2,config.node3,config.node4,config.node5]
    # create segmentation model with pretrained encoder
    if config.segmentation_model in ['Unet','UnetPlusPlus','MAnet']:
        model = segmentation_model(
                encoder_name = config.encoder,
                encoder_weights = encoder_weights,
                classes = 1,
                activation = None,
                decoder_channels = tuple(decoder_channels),
                encoder_depth = len(decoder_channels),
        )
    else:
        model = segmentation_model(
                encoder_name = config.encoder,
                encoder_weights = encoder_weights,
                classes = 1,
        )
    wandb.watch(model)
    model.to(device = config.device)
    train_dataset = RoadDataset(DATA_DIR,
                                split = "train",
                                transform = get_augmentation(augmentation = config.augmenter,
                                                             img_size = config.img_size
                                                             ),
                                step_factor = config.step_factor,
                                test_split = config.test_split,
                                seed = config.seed,
                                external_deep = config.external_deep,
                                external_massa = config.external_massa,
                                external_epfl = config.external_epfl,
                                external_roadtracer = config.external_roadtracer,
                                preprocess = preprocessing_fn,
                                )

    valid_dataset = RoadDataset(DATA_DIR,
                                split = "val",
                                transform = get_augmentation(augmentation = "Validation", img_size = config.img_size),
                                step_factor = config.step_factor,
                                test_split = config.test_split,
                                seed = config.seed,
                                preprocess = preprocessing_fn,
                                )

    train_loader = DataLoader(train_dataset,
                              batch_size = config.batch_size,
                              shuffle = True,
                              num_workers = config.num_workers,
                              pin_memory = True,
                              worker_init_fn = seed_worker,
                              )
    valid_loader = DataLoader(valid_dataset,
                              batch_size = 20,
                              shuffle = False,
                              num_workers = config.num_workers,
                              pin_memory = True,
                              worker_init_fn = seed_worker,
                              )

    loss = get_loss_function(config)
    metrics = ["f1_score",
               "iou_score",
               "accuracy",
               ]
    optimizer = getattr(torch.optim, config.optimizer)
    optimizer = optimizer([
            dict(params = model.parameters(),
                 lr = config.learning_rate
                 ),
    ]
    )


    if config.mode == "max":
        best_monitor = 0
    else:
        best_monitor = 1
    epoch =0
    counter=0

    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer,
                                                           mode = config.mode,
                                                           patience = config.patience,
                                                           verbose = True,
                                                           threshold = 1e-6,
                                                           factor = 0.5
                                                           )

    # create epoch runners
    # it is a simple loop of iterating over dataloader`s samples
    train_epoch = TrainEpoch(
            model,
            loss = loss,
            metrics = metrics,
            optimizer = optimizer,
            device = config.device,
            verbose = True,
    )

    valid_epoch = ValidEpoch(
            model,
            loss = loss,
            metrics = metrics,
            device = config.device,
            verbose = True,
    )
    # train model for N epochs
    # train model for N epochs
    for i in range(epoch, config.epochs):
        print('\nEpoch: {}'.format(i))
        train_logs = train_epoch.run(train_loader, config)
        valid_logs = valid_epoch.run(valid_loader, config)
        if config.artifacts:
            # create a wandb Artifact to version each test step separately
            test_data_at = wandb.Artifact("test_samples_" + str(wandb.run.id), type = "predictions")
            # create a wandb.Table() in which to store predictions for each test step
            columns = ["id", "image", "truth", "guess", "accuracy", "iou", "f1score"]
            test_table = wandb.Table(columns = columns)

        scheduler.step(valid_logs[config.monitor])
        # do something (save model, change lr, etc.)
        if (config.mode == "max" and best_monitor < valid_logs[config.monitor]) or (
                config.mode == "min" and best_monitor > valid_logs[config.monitor]):
            best_monitor = valid_logs[config.monitor]
            counter = 0
            wandb.log({f"best_{config.monitor}": best_monitor}
                      )

        else:
            counter += 1
        train_logs = {f'train_{k}': v for k, v in train_logs.items()}
        valid_logs = {f'val_{k}': v for k, v in valid_logs.items()}
        wandb.log(train_logs)
        wandb.log(valid_logs)
        wandb.log({"epoch": i,
                   "lr":    optimizer.param_groups[0]['lr'],
                   }
                  )


        if counter >= int(config.patience * 0.5) + config.patience + 1:
            break

if __name__ == '__main__':
    # Define pahts
    PROJECT_DIR = Path(__file__).parent.parent.parent
    DATA_DIR = os.path.join(PROJECT_DIR, "data/")
    CONFIG_DIR = os.path.join(PROJECT_DIR, "config/")
    WANDB_DIR = os.path.join(PROJECT_DIR)
    dotenv_path = os.path.join(PROJECT_DIR, '.env')
    dotenv.load_dotenv(dotenv_path)

    # Define Wandb Envs
    os.environ['WANDB_MODE'] = 'online'
    os.environ['WANDB_DIR'] = WANDB_DIR
    os.environ["WANDB_CONFIG_DIR"] = WANDB_DIR
    os.environ['WANDB_CACHE_DIR'] = WANDB_DIR

    parser = argparse.ArgumentParser()
    parser.add_argument('-lo', '--loss', type=str, default="SoftBCE", help="Loss Function || "
                                                                           "Default: SoftBCE || "
                                                                           "Options: SoftBCE,Dice,Jaccard,Lovasz,CLDice,Tversky,BCECLDice,Focal")
    parser.add_argument('-al', '--alpha', type=float, default=0.1, help="Alpha Ratio || "
                                                                        "Defaul: 0.1 || "
                                                                        "Options: Range(0,1) || "
                                                                        "Description: Relevant for CLDice and BCECLDice loss, defines ratio of CLDice to Dice or BCE")
    parser.add_argument('-en', '--encoder', type=str, default="swin_t", help="Encoder || "
                                                                             "Default: timm-efficientnet-b6 || "
                                                                             "Options: https://smp.readthedocs.io/en/latest/encoders.html")
    parser.add_argument('-au', '--augmenter', type=str, default="Combine4", help="Augmentation Policy || "
                                                                                 "Default: Combine4 || "
                                                                                 "Options: src.utils.utils.get_augmentation || "
                                                                                 "Description: We use stacks of Albumentations transforms, you can set your own at src.utils.utils.get_augmentation")
    parser.add_argument('-ew', '--encoder_weights', type=str, default="imagenet", help="Pretrained Weights || "
                                                                                       "Default: imagenet || "
                                                                                       "Options: None, imagenet ... https://smp.readthedocs.io/en/latest/encoders.html || "
                                                                                       "Description: see what weights are compatible with what encoder at provided link")
    parser.add_argument('-sm', '--segmentation_model', type=str, default="Unet", help="Segmentation Model|| "
                                                                                      "Default: Unet || "
                                                                                      "Options: Unet, UnetPlusPlus etc ... https://smp.readthedocs.io/en/latest/models.html")
    parser.add_argument('-no1', '--node1', type=int, default=1024, help="First Decoder Channel || "
                                                                        "Default: 1024 || ")
    parser.add_argument('-no2', '--node2', type=int, default=256, help="First Decoder Channel || "
                                                                       "Default: 256 || ")
    parser.add_argument('-no3', '--node3', type=int, default=128, help="First Decoder Channel || "
                                                                       "Default: 128 || ")
    parser.add_argument('-no4', '--node4', type=int, default=64, help="First Decoder Channel || "
                                                                      "Default: 64 || ")
    parser.add_argument('-no5', '--node5', type=int, default=None, help="First Decoder Channel || "
                                                                        "Default: 16 || ")
    parser.add_argument('-op', '--optimizer', type=str, default="NAdam", help="Optimizer || "
                                                                              "Default: NAdam || "
                                                                              "Options: Adam,NAdam, SGD, Adadelta, Adamax,Adagrad,SparseAdam, RMSprop")
    parser.add_argument('-lr', '--learning_rate', type=float, default=0.0008, help="Learning Rate || "
                                                                                   "Default: 0.0008")
    parser.add_argument('-is', '--img_size', type=int, default=416, help="Image Size || "
                                                                         "Default: 416 || "
                                                                         "Options: img_size / 32 = int! || "
                                                                         "Description: Must be fitting for Unet, int*32")
    parser.add_argument('-ep', '--epochs', type=int, default=200, help="Epochs || "
                                                                       "Default: 200")
    parser.add_argument('-bs', '--batch_size', type=int, default=6, help="Batch Size || "
                                                                         "Default: 6 || "
                                                                         "Description: Care Encoder, Segmentation Model, Image Size and Batch Size define your GPU memory usage")
    parser.add_argument('-sf', '--step_factor', type=float, default=2.0, help="Step Factor || "
                                                                              "Default: 2 || "
                                                                              "Options: range(0.1-10) || "
                                                                              "Description: If <1 Now whole dataset is used for training, if >1 dataset is copied and images are used multiple times per epoch, only makes sense if augmented")
    parser.add_argument('-ts', '--test_split', type=float, default=0.15, help="Test Split || "
                                                                              "Default: 0.15")
    parser.add_argument('-at', '--attention_type', type=str, default=None)
    parser.add_argument('-sd', '--seed', type=int, default=42, help="Seed || "
                                                                    "Default: 42")
    parser.add_argument('-dv', '--device', type=str, default='cuda:0', help="Device || "
                                                                            "Default: cuda:0 || "
                                                                            "Description: Your GPU")
    parser.add_argument('-mt', '--monitor', type=str, default='accuracy', help="Monitor Metric || "
                                                                               "Default: accuracy || "
                                                                               "Options: accuracy,iou,f1score,cldice")
    parser.add_argument('-md', '--mode', type=str, default="max", help="Monitor Mode || "
                                                                       "Default: max || "
                                                                       "Description: Depends on your Monitor Metric")
    parser.add_argument('-nw', '--num_workers', type=int, default=4, help="Num Workers || "
                                                                          "Default: 4")
    parser.add_argument('-pt', '--patience', type=int, default=21, help="Patience || "
                                                                        "Default: 21 || "
                                                                        "Description: Epochs until LR reduction, patience + patience//2 until ealry stopping if no increase in Monitor Metric")
    parser.add_argument('-exd', '--external_deep', type=bool, default=False, help="External Data Deepglobe || "
                                                                                  "Default: False || ")
    parser.add_argument('-exm', '--external_massa', type=bool, default=False, help="External Data Massachussets || "
                                                                                   "Default: False || ")
    parser.add_argument('-exe', '--external_epfl', type=bool, default=False, help="External Data EPFL || "
                                                                                  "Default: False || ")
    parser.add_argument('-exr', '--external_roadtracer', type=bool, default=False, help="External Data EPFL || "
                                                                                        "Default: False || ")
    parser.add_argument('-af', '--artifacts', type=bool, default=True, help="Wandb Artifacts || "
                                                                            "Default: True || "
                                                                            "Description: Save predicted masks in Wandb Tables")
    parser.add_argument('-th', '--threshold', type=float, default=0.5, help="Binary mask cutoff || "
                                                                            "Default: 0.5 || "
                                                                            "Description: Will be evaluated within run later automatically")
    parser.add_argument('-if', '--inference', type=bool, default=True, help="Inference || "
                                                                            "Default: True || "
                                                                            "Description: Use best model on Test datset and log in Wandb")
    parser.add_argument('-sb', '--submission', type=bool, default=True, help="Submission || "
                                                                             "Default: True || "
                                                                             "Description: Create Submission directory in Wandb Directory with predicted masks on which mask_to_submission can be applied")
    parser.add_argument('-id', '--id', type=str, default=None, help="Wandb ID || "
                                                                    "Default: None ||"
                                                                    "Description: Makes rune ")
    parser.add_argument('-ow', '--overwrite', type=str, default=False, help="Overwrite config || "
                                                                            "Default: False ||"
                                                                            "Description: If you want to break actual run and resume with different args eg. patience or data or whatever")

    args = parser.parse_args()
    # Reproducibility
    seed_all(seed=args.seed)

    wandb.init(config=args, project="Road",
               dir=WANDB_DIR,
               anonymous="allow",
               resume="allow",
               id=args.id,
               )
    config = wandb.config
    main()
