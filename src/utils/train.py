#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : Daniel Guthruf
# Created Date: 27.06.2022
# =============================================================================
"""
Filename:           train.py
Description:        Trains segmentation model
Main Libs:          Wandb,segmentation_models_pytorch
Long Description:   Choose encoder,decoder,loss,optimizer etc... via
                    config.yaml file
                    Not only is the basic dataset available, with make_datasets
                    files in src/data you can generate deepglobe, massachues
                    roadtracing, and epfl
                    for more data. Everything is logged in wandb.
State:              dev
"""
# =============================================================================
# Imports
# =============================================================================
import warnings

warnings.filterwarnings("ignore", category = FutureWarning)
import torch.backends.cudnn as cudnn

cudnn.benchmark = True

import os
import random
import wandb
import numpy as np
import torch
import argparse
import dotenv
from pathlib import Path
from torch.utils.data import DataLoader
import segmentation_models_pytorch as smp
from src.utils.augmentations import get_augmentation
from src.utils.train_modules import TrainEpoch, ValidEpoch
from src.utils.data_modules import RoadDataset, RoadDataTestSet, get_preprocessing
from src.utils.metrics import get_loss_function
from src.utils.logger import log_threshold, log_submission_predictions, log_validation_artifacts
from src.models.my_unet import MyUNet

def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2 ** 32
    np.random.seed(worker_seed)
    random.seed(worker_seed)


def seed_all(seed: int) -> None:
    """
    Setting all the relevant seed to make run reproducible
    @param seed:
    """
    if not seed:
        seed = 10

    print("[ Using Seed : ", seed, " ]")

    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.cuda.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)
    # torch.use_deterministic_algorithms(True)
    g = torch.Generator()
    g.manual_seed(0)
    # torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False


def main():
    decoder_channels = [channel for channel in config.decoder_channels if channel]
    if config.segmentation_model !='BaseUnet':
        segmentation_model = getattr(smp, config.segmentation_model)
        if config.encoder_weights=="imagenet":
            preprocessing_fn = smp.encoders.get_preprocessing_fn(config.encoder, config.encoder_weights)
            preprocessing_fn = get_preprocessing(preprocessing_fn)
            encoder_weights = config.encoder_weights
        else:
            preprocessing_fn = None
            encoder_weights=None
        # create segmentation model with pretrained encoder
        if segmentation_model in ['Unet', 'UnetPlusPlus', 'MAnet']:
            model = segmentation_model(
                    encoder_name = config.encoder,
                    encoder_weights = encoder_weights,
                    classes = 1,
                    activation = None,
                    decoder_channels = tuple(decoder_channels),
                    encoder_depth = len(decoder_channels),
            )
        else:
            model = segmentation_model(
                    encoder_name = config.encoder,
                    encoder_weights = encoder_weights,
                    classes = 1,
            )
    elif config.segmentation_model == 'BaseUnet':
        preprocessing_fn = None
        encoder_weights = None
        # smp.encoders.encoders["BaseUnet"] = {
        #                                     "encoder": BaseUnet,  # encoder class here
        #                                     "pretrained_settings": {
        #                                             },
        #                                     "params":{
        #                                             },
        #                                       }

        # model = smp.Unet(encoder_name = "BaseUnet",
        #                  encoder_weights = encoder_weights,
        #                  classes = 1,
        #                  activation = None,
        #                  decoder_channels = tuple(decoder_channels),
        #                  encoder_depth = len(decoder_channels),
        #                  )
        model = MyUNet(n_channels = 3,n_classes = 1,bilinear = True)
    else:
        raise Exception('Choose a viable Encoder/Decoder combination!')

    wandb.watch(model)
    model.to(device = config.device)
    train_dataset = RoadDataset(DATA_DIR,
                                split = "train",
                                transform = get_augmentation(augmentation = config.augmenter,
                                                             img_size = config.img_size
                                                             ),
                                step_factor = config.step_factor,
                                test_split = config.test_split,
                                seed = config.seed,
                                external_deep = config.external_deep,
                                external_massa = config.external_massa,
                                external_epfl = config.external_epfl,
                                external_roadtracer = config.external_roadtracer,
                                preprocess = preprocessing_fn,
                                )

    valid_dataset = RoadDataset(DATA_DIR,
                                split = "val",
                                transform = get_augmentation(augmentation = "Validation", img_size = config.img_size),
                                step_factor = config.step_factor,
                                test_split = config.test_split,
                                seed = config.seed,
                                preprocess = preprocessing_fn,
                                )

    train_loader = DataLoader(train_dataset,
                              batch_size = config.batch_size,
                              shuffle = True,
                              num_workers = config.num_workers,
                              pin_memory = True,
                              worker_init_fn = seed_worker,
                              )
    valid_loader = DataLoader(valid_dataset,
                              batch_size = 20,
                              shuffle = False,
                              num_workers = 0,
                              pin_memory = True,
                              worker_init_fn = seed_worker,
                              )

    loss = get_loss_function(config)
    metrics = ["f1_score",
               "iou_score",
               "accuracy",
               ]
    optimizer = getattr(torch.optim, config.optimizer)
    optimizer = optimizer([
            dict(params = model.parameters(),
                 lr = config.learning_rate
                 ),
    ]
    )

    if wandb.run.resumed:
        checkpoint = torch.load(CHECKPOINT_PATH)
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        for g in optimizer.param_groups:
            g['lr'] = config.learning_rate
        epoch = checkpoint['epoch'] + 1
        best_monitor = checkpoint[f"best_{config.monitor}"]
    else:
        if config.mode == "max":
            best_monitor = 0
        else:
            best_monitor = 1
        epoch =0
    counter=0

    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer,
                                                           mode = config.mode,
                                                           patience = config.patience,
                                                           verbose = True,
                                                           threshold = 1e-6,
                                                           factor = 0.5
                                                           )

    # create epoch runners
    # it is a simple loop of iterating over dataloader`s samples
    train_epoch = TrainEpoch(
            model,
            loss = loss,
            metrics = metrics,
            optimizer = optimizer,
            device = config.device,
            verbose = True,
    )

    valid_epoch = ValidEpoch(
            model,
            loss = loss,
            metrics = metrics,
            device = config.device,
            verbose = True,
    )
    # train model for N epochs
    # train model for N epochs
    for i in range(epoch, config.epochs):
        print('\nEpoch: {}'.format(i))
        train_logs = train_epoch.run(train_loader, config)
        valid_logs = valid_epoch.run(valid_loader, config)
        if config.artifacts:
            # create a wandb Artifact to version each test step separately
            test_data_at = wandb.Artifact("test_samples_" + str(wandb.run.id), type = "predictions")
            # create a wandb.Table() in which to store predictions for each test step
            columns = ["id", "image", "truth", "guess", "accuracy", "iou", "f1score"]
            test_table = wandb.Table(columns = columns)

        scheduler.step(valid_logs[config.monitor])
        # do something (save model, change lr, etc.)
        if (config.mode == "max" and best_monitor < valid_logs[config.monitor]) or (
                config.mode == "min" and best_monitor > valid_logs[config.monitor]):
            best_monitor = valid_logs[config.monitor]
            counter = 0
            if config.artifacts:
                # wandb_predictions(dataset=valid_loader, model=model)
                log_validation_artifacts(test_table = test_table, dataset = valid_dataset, model = model)
                test_data_at.add(test_table, "predictions")
                wandb.run.log_artifact(test_data_at)
                torch.save(model.state_dict(), os.path.join(wandb.run.dir, "model.pt"))
                torch.save(model, os.path.join(wandb.run.dir, "model_full.pt"))
                print("Model saved!")
            wandb.log({f"best_{config.monitor}": best_monitor}
                      )

        else:
            counter += 1
        train_logs = {f'train_{k}': v for k, v in train_logs.items()}
        valid_logs = {f'val_{k}': v for k, v in valid_logs.items()}
        wandb.log(train_logs)
        wandb.log(valid_logs)
        wandb.log({"epoch": i,
                   "lr":    optimizer.param_groups[0]['lr'],
                   }
                  )

        torch.save({
                'epoch':                  i,
                'model_state_dict':       model.state_dict(),
                'optimizer_state_dict':   optimizer.state_dict(),
                'scheduler_state_dict':   scheduler.state_dict(),
                f'best_{config.monitor}': best_monitor,
                'config_dictionary':      config_dictionary,
        }, CHECKPOINT_PATH
        )
        if counter >= config.patience*2:
            break
    if config.artifacts:
        test_dataset = RoadDataTestSet(DATA_DIR,
                                       transform = get_augmentation(augmentation = "Validation",
                                                                    img_size = config.img_size
                                                                    ),
                                       seed = config.seed,
                                       with_size = False,
                                       preprocess = preprocessing_fn,
                                       )
        valid_dataset = RoadDataset(DATA_DIR,
                                    split = "val",
                                    transform = get_augmentation(augmentation = "Validation",
                                                                 img_size = config.img_size
                                                                 ),
                                    step_factor = config.step_factor,
                                    test_split = 0.99,
                                    seed = config.seed,
                                    preprocess = preprocessing_fn,
                                    )
        # Get best threshold value
        model.load_state_dict(torch.load(os.path.join(wandb.run.dir, "model.pt")))
        model.eval()
        log_threshold(model, wandb, valid_dataset, resolution = 30)
        _ = log_submission_predictions(model, wandb, test_dataset)


if __name__ == '__main__':
    # Define pahts
    PROJECT_DIR = Path(__file__).parent.parent.parent
    DATA_DIR = os.path.join(PROJECT_DIR, "data/")
    CONFIG_DIR = os.path.join(PROJECT_DIR, "config/")
    WANDB_DIR = os.path.join(PROJECT_DIR)
    dotenv_path = os.path.join(PROJECT_DIR, '.env')
    dotenv.load_dotenv(dotenv_path)


    # Reproducibility
    seed_all(seed = 0)


    parser = argparse.ArgumentParser()
    # parser.add_argument('--resume',action=argparse.BooleanOptionalAction)
    parser.add_argument('--config', default = 'default')
    parser.add_argument('--id', default = None)

    args = parser.parse_args()


    config_dictionary = f"{args.config}.yaml"
    if not args.id:
        args.id = wandb.util.generate_id()
        CHECKPOINT_PATH = os.path.join(PROJECT_DIR, f'wandb/checkpoint/checkpoint{args.id}.pt')

        print(f"Created ID:{args.id}")
        wandb.init(config = os.path.join(PROJECT_DIR,f'wandb/config/{config_dictionary}'),
                   resume = "allow",
                   id = args.id,
                   allow_val_change = True,
                   )
    else:
        CHECKPOINT_PATH = os.path.join(PROJECT_DIR, f'wandb/checkpoint/checkpoint{args.id}.pt')
        if args.config == "default":
            checkpoint = torch.load(CHECKPOINT_PATH)
            config_dictionary = checkpoint["config_dictionary"]
            wandb.init(config = config_dictionary,
                       anonymous = "allow",
                       resume = "allow",
                       id = args.id,
                       allow_val_change = True,
                       )
        elif args.config == "overwrite":
            wandb.init(config = os.path.join(PROJECT_DIR, f'wandb/config/{config_dictionary}'),
                       resume = "allow",
                       id = args.id,
                       allow_val_change = True,
                       )
    if args.id:
        wandb.config.update({"id":args.id},allow_val_change = True,)
    config = wandb.config
    main()
