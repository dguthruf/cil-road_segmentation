# -*- coding: utf-8 -*-
import os
import warnings

warnings.filterwarnings("ignore")
import cv2
import numpy as np
import torch
import torch.backends.cudnn as cudnn
from torch.utils.data import Dataset, DataLoader
from typing import Tuple, List

cudnn.benchmark = True
import random
import albumentations as A
from albumentations.pytorch import ToTensorV2

device = torch.device('cuda:0') if torch.cuda.is_available() else torch.device('cpu')


def preprocess_mask(mask: torch.FloatTensor) -> torch.FloatTensor:
    "This function takes a mask and converts it to a uint8 and float32,"
    "then sets the bits that are 1.0 and 0.0, respectively."
    mask = (mask > 0).astype(np.uint8) * 255
    mask = mask.astype(np.float32)
    mask[mask == 255.0] = 1.0
    mask[mask == 0.0] = 0.0
    return mask


class RoadDataset(Dataset):
    def __init__(self,
                 data_path: str,
                 split: float,
                 test_split: float = 0.15,
                 preprocess: bool = False,
                 step_factor: float = 1.0,
                 transform=None,
                 external_deep: bool = False,
                 external_massa: bool = False,
                 external_epfl: bool = False,
                 external_roadtracer: bool = False,
                 seed: int = 42,
                 ) -> None:
        """
        Defines filenames used to pick each example
        @param data_path: path to data folder
        @param split:  train or validation
        @param test_split: test_split
        @param preprocess: use normalization if pretrained weights
        @param step_factor: use original dataset multiple times (e.g. case of augmentations)
        @param transform: Albumentation policy
        @param external_deep: Use Deepglobe dataset
        @param external_massa: Use Massachussets dataset
        @param external_epfl: Use EPFL dataset
        @param external_roadtracer: Use Roadtracing dataset
        @param seed: set seed for reproducibility
        """
        self.data_path = data_path
        self.images_directory = data_path + "/raw/training/images/"
        self.masks_directory = data_path + "/raw/training/groundtruth/"
        self.external_roadtracer = external_roadtracer
        self.test_split = test_split
        self.seed = seed
        self.split = split
        self.transform = transform
        self.preprocess = preprocess
        self.step_factor = step_factor
        self.externals = [external_deep, external_massa, external_epfl, external_roadtracer]
        self.external_strings = ["deepglobe", "massachusetts", "epfl", "roadtracing"]

        # Get Raw Data filenmaes
        file_names = self.__raw_data__()
        # External DATA
        for external_bool, external_string in zip(self.externals, self.external_strings):
            if external_bool:
                file_names = self.__external_data__(external_string, file_names)
        self.images_filenames = file_names

    def __len__(self) -> int:
        return len(self.images_filenames)

    def __getitem__(self, idx: int) -> Tuple[torch.FloatTensor, torch.FloatTensor]:
        """
        Applies Transform, preprocess and prepares image,mask tuple for model
        @param idx: index number
        @return: image, mask as tensor
        """
        image_filename = self.images_filenames[idx]
        image = cv2.imread(image_filename)
        mask = cv2.imread(image_filename.replace("images", "groundtruth"), cv2.IMREAD_UNCHANGED)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        mask = preprocess_mask(mask)
        if self.transform is not None:
            transformed = self.transform(image=image, mask=mask)
            image = transformed["image"]
            mask = transformed["mask"]
        # apply preprocessing
        if self.preprocess is not None:
            sample = self.preprocess(image=image, mask=mask)
            image, mask = sample['image'], sample['mask']
        else:
            tensors = self.to_tensor()(image=image, mask=mask)
            image = tensors["image"]
            mask = tensors["mask"]
        image = image.type(torch.float32)
        mask = mask.type(torch.float32)
        if len(mask.size()) == 2:
            mask = torch.unsqueeze(mask, 0)
        return image, mask

    def __raw_data__(self) -> List:
        """
        Prepares list of filenames for __getitem__
        @return: list of filename strings
        """
        # Get filenames from main raw directory
        internal_fns = list(sorted(os.listdir(self.images_directory)))
        internal_fns = [os.path.join(self.images_directory, i) for i in internal_fns if
                        cv2.imread(os.path.join(self.images_directory, i)) is not None]

        random.seed(self.seed)
        random.shuffle(internal_fns)
        train_fns = internal_fns[0:np.int32((1 - self.test_split) * len(internal_fns))]
        val_fns = internal_fns[np.int32((1 - self.test_split) * len(internal_fns)):-1]

        if self.split == "train":
            fns = []
            if self.step_factor > 1:
                temp_filenames = list(train_fns)
                for i in range(int(self.step_factor)):
                    for element in temp_filenames:
                        fns.append(element)
            else:
                fns = train_fns[0:int(len(train_fns) * self.step_factor)]
        else:
            fns = val_fns
        return fns

    def __external_data__(self, data_name: str, fns: List) -> List:
        """
        @param data_name: external data_origin name , deepglobe,massa etc
        @param fns: already existing filenames, which will be used
        @return: extended list of filenames
        """
        data_path = self.data_path + f"/external/{data_name}/"
        images_directory_external = data_path + "processed/images/"
        external_fns = list(sorted(os.listdir(images_directory_external)))
        external_fns = [os.path.join(images_directory_external, i) for i in external_fns if
                        cv2.imread(os.path.join(images_directory_external, i)) is not None]

        random.seed(self.seed)
        random.shuffle(external_fns)
        fns.extend(external_fns)
        random.shuffle(fns)
        return fns

    def to_tensor(self):
        _transform = [
            ToTensorV2()
        ]
        return A.Compose(_transform)


def get_preprocessing(preprocessing_fn):
    """Construct preprocessing transform

    Args:
        preprocessing_fn (callbale): data normalization function
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose

    """

    _transform = [
        A.Lambda(image=preprocessing_fn),
        # A.Lambda(image=to_tensor, mask=to_tensor),
        ToTensorV2()
    ]
    return A.Compose(_transform)


class RoadDatasetInference(Dataset):
    def __init__(self, images_filenames, images_directory, transform=None):
        self.images_filenames = images_filenames
        self.images_directory = images_directory + "/raw/training/images"
        self.transform = transform

    def __len__(self):
        return len(self.images_filenames)

    def __getitem__(self, idx):
        image_filename = self.images_filenames[idx]
        image = cv2.imread(os.path.join(self.images_directory, image_filename))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        original_size = tuple(image.shape[:2])
        if self.transform is not None:
            transformed = self.transform(image=image)
            image = transformed["image"]
        return image, original_size


def predict(model, params, test_dataset, batch_size, threshold=0.5):
    test_loader = DataLoader(
        test_dataset, batch_size=batch_size, shuffle=False, num_workers=0, pin_memory=True,
    )
    model.eval()
    predictions = []
    with torch.no_grad():
        for images, (original_heights, original_widths) in test_loader:
            images = images.to(params["device"], non_blocking=True)
            output = model(images)
            probabilities = torch.sigmoid(output.squeeze(1))
            predicted_masks = (probabilities >= threshold).float() * 1
            predicted_masks = predicted_masks.cpu().numpy()
            for predicted_mask, original_height, original_width in zip(
                    predicted_masks, original_heights.numpy(), original_widths.numpy()
            ):
                predictions.append((predicted_mask, original_height, original_width))
    return predictions


class RoadDataTestSet(Dataset):
    def __init__(self,
                 data_path,
                 transform=None,
                 seed=42,
                 with_size=True,
                 preprocess=None,
                 ):
        self.images_directory = data_path + "/raw/test/images/"
        self.seed = seed
        self.transform = transform
        self.with_size = with_size
        images_filenames = list(sorted(os.listdir(self.images_directory)))
        correct_images_filenames = [i for i in images_filenames if
                                    cv2.imread(os.path.join(self.images_directory, i)) is not None]

        self.images_filenames = correct_images_filenames
        self.preprocess = preprocess

    def __len__(self):
        return len(self.images_filenames)

    def __getitem__(self, idx):
        image_filename = self.images_filenames[idx]
        image = cv2.imread(os.path.join(self.images_directory, image_filename))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        original_size = tuple(image.shape[:2])
        if self.transform is not None:
            transformed = self.transform(image=image)
            image = transformed["image"]
        # apply preprocessing
        if self.preprocess is not None:
            sample = self.preprocess(image=image)
            image = sample['image']
        else:
            try:
                tensors = self.to_tensor()(image=image)
                image = tensors["image"]
            except:
                pass
        image = image.to(torch.float32)

        if self.with_size:
            return image, original_size
        else:
            return image

    def to_tensor(self):
        _transform = [
            ToTensorV2()
        ]
        return A.Compose(_transform)
