import numpy as np
import segmentation_models_pytorch as smp
import torch
import torch.nn as nn
import torch.nn.functional as F
from skimage.morphology import skeletonize, skeletonize_3d


class soft_cldice(nn.Module):
    def __init__(self, iter_=3, smooth = 1.):
        super(soft_cldice, self).__init__()
        self.iter = iter_
        self.smooth = smooth

    def forward(self,y_pred, y_true):
        y_pred = F.logsigmoid(y_pred).exp()
        skel_pred = soft_skel(y_pred, self.iter)
        skel_true = soft_skel(y_true, self.iter)
        tprec = norm_intersection(skel_pred, y_true)
        tsens = norm_intersection(skel_true, y_pred)
        cl_dice = 1. - 2.0 * (tprec * tsens) / (tprec + tsens)
        return cl_dice.mean()


def soft_dice(y_pred, y_true):
    """[function to compute dice loss]
    Args:
        y_true ([float32]): [ground truth image]
        y_pred ([float32]): [predicted image]
    Returns:
        [float32]: [loss value]
    """
    smooth = 1
    intersection = torch.sum((y_true * y_pred)[:,1:,...])
    coeff = (2. *  intersection + smooth) / (torch.sum(y_true[:,1:,...]) + torch.sum(y_pred[:,1:,...]) + smooth)
    return (1. - coeff)

def dice_loss(pred, target):
    '''
    inputs shape  (batch, channel, height, width).
    calculate dice loss per batch and channel of sample.
    E.g. if batch shape is [64, 1, 128, 128] -> [64, 1]
    '''
    smooth = 1.
    iflat = pred.view(*pred.shape[:2], -1) #batch, channel, -1
    tflat = target.view(*target.shape[:2], -1)
    intersection = (iflat * tflat).sum(-1)
    return 1.0-((2. * intersection + smooth) /
              (iflat.sum(-1) + tflat.sum(-1) + smooth))

class soft_dice_cldice(nn.Module):
    def __init__(self, iter_=3, alpha=0.5, smooth = 1.):
        super(soft_dice_cldice, self).__init__()
        self.iter = iter_
        self.smooth = smooth
        self.alpha = alpha

    def forward(self,y_pred, y_true):
        y_pred = F.logsigmoid(y_pred).exp()
        dice = dice_loss(y_pred, y_true)
        skel_pred = soft_skel(y_pred, self.iter)
        skel_true = soft_skel(y_true, self.iter)
        tprec = norm_intersection(skel_pred, y_true)
        tsens = norm_intersection(skel_true, y_pred)
        cl_dice = 1. - 2.0 * (tprec * tsens) / (tprec + tsens)
        return ((1.0-self.alpha)*dice+self.alpha*cl_dice).mean()

class soft_bce_cldice(nn.Module):
    def __init__(self, iter_=3, alpha=0.5, smooth = 1.):
        super(soft_bce_cldice, self).__init__()
        self.iter = iter_
        self.smooth = smooth
        self.alpha = alpha

    def forward(self,y_pred, y_true):
        bce_score = F.binary_cross_entropy_with_logits(
            y_pred,
            y_true,
            None,
            pos_weight=None,
            reduction="none",
        )
        y_pred = F.logsigmoid(y_pred).exp()
        bce_score=bce_score.mean()
        skel_pred = soft_skel(y_pred, self.iter)
        skel_true = soft_skel(y_true, self.iter)
        tprec = norm_intersection(skel_pred, y_true)
        tsens = norm_intersection(skel_true, y_pred)
        cl_dice = 1. - 2.0 * (tprec * tsens) / (tprec + tsens)
        return ((1.0-self.alpha)*bce_score+self.alpha*cl_dice).mean()




def soft_erode(img):
    if len(img.shape)==4:
        p1 = -F.max_pool2d(-img, (3,1), (1,1), (1,0))
        p2 = -F.max_pool2d(-img, (1,3), (1,1), (0,1))
        return torch.min(p1,p2)
    elif len(img.shape)==5:
        p1 = -F.max_pool3d(-img,(3,1,1),(1,1,1),(1,0,0))
        p2 = -F.max_pool3d(-img,(1,3,1),(1,1,1),(0,1,0))
        p3 = -F.max_pool3d(-img,(1,1,3),(1,1,1),(0,0,1))
        return torch.min(torch.min(p1, p2), p3)


def soft_dilate(img):
    if len(img.shape)==4:
        return F.max_pool2d(img, (3,3), (1,1), (1,1))
    elif len(img.shape)==5:
        return F.max_pool3d(img,(3,3,3),(1,1,1),(1,1,1))


def soft_open(img):
    return soft_dilate(soft_erode(img))


def soft_skel(img, iter_):
    img1  =  soft_open(img)
    skel  =  F.relu(img-img1)
    for j in range(iter_):
        img  =  soft_erode(img)
        img1  =  soft_open(img)
        delta  =  F.relu(img-img1)
        skel  =  skel +  F.relu(delta-skel*delta)
    return skel


def cl_score(v, s):
    """[this function computes the skeleton volume overlap]
    Args:
        v ([bool]): [image]
        s ([bool]): [skeleton]
    Returns:
        [float]: [computed skeleton volume intersection]
    """
    return np.sum(v*s)/np.sum(s)


def clDice(v_p, v_l):
    """[this function computes the cldice metric]
    Args:
        v_p ([bool]): [predicted image]
        v_l ([bool]): [ground truth image]
    Returns:
        [float]: [cldice metric]
    """
    if len(v_p.shape)==2:
        tprec = cl_score(v_p,skeletonize(v_l))
        tsens = cl_score(v_l,skeletonize(v_p))
    elif len(v_p.shape)==3:
        tprec = cl_score(v_p,skeletonize_3d(v_l))
        tsens = cl_score(v_l,skeletonize_3d(v_p))
    return 2*tprec*tsens/(tprec+tsens)

def norm_intersection(center_line, vessel):
    '''
    inputs shape  (batch, channel, height, width)
    intersection formalized by first ares
    x - suppose to be centerline of vessel (pred or gt) and y - is vessel (pred or gt)
    '''
    smooth = 1.
    clf = center_line.view(*center_line.shape[:2], -1)
    vf = vessel.view(*vessel.shape[:2], -1)
    intersection = (clf * vf).sum(-1)
    return (intersection + smooth) / (clf.sum(-1) + smooth)

def get_loss_function(config):
    loss = config.loss
    if loss == "Jaccard":
        loss_fn = smp.losses.JaccardLoss(mode="binary", from_logits = True)
    elif loss == "Dice":
        loss_fn = smp.losses.DiceLoss(mode="binary", from_logits = True)
    elif loss == "Tversky":
        loss_fn = smp.losses.TverskyLoss(mode="binary", from_logits = True,alpha=0.1, beta=0.90)
    elif loss == "Focal":
        loss_fn = smp.losses.FocalLoss(mode="binary",gamma=4)
    elif loss == "Lovasz":
        loss_fn = smp.losses.LovaszLoss(mode="binary", from_logits = True)
    elif loss == "SoftBCE":
        loss_fn = smp.losses.SoftBCEWithLogitsLoss()
    elif loss == "MCC":
        loss_fn = smp.losses.MCCLoss()
    return loss_fn