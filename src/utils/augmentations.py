import random

import albumentations as A
import numpy as np
from skimage import morphology


def custom_aug_mask(mask,max_value=8,**kwargs):
    dil = random.randrange(max_value)
    ero = random.randrange(max_value)
    mask = morphology.binary_dilation(mask,morphology.selem.disk(dil))
    return morphology.binary_erosion(mask,morphology.selem.disk(ero)).astype(np.float32)
def get_augmentation(augmentation,img_size):
    transform = A.Compose([
        A.PadIfNeeded(min_height=img_size, min_width=img_size,p=1.0),
        A.Resize(height=img_size,width=img_size)
    ])
    if augmentation == "ResizedCrop":
        transform = A.Compose([
            transform,
            A.RandomResizedCrop(img_size, img_size,
                                p=0.75,
                                scale=(0.5, 1.2),
                                ratio=(0.85, 1.15))]
        )
    elif augmentation == "ResizedCrop2":
        transform = A.Compose([
            transform,
            A.RandomResizedCrop(img_size, img_size,
                                p=0.75,
                                scale=(0.2, 1.6),
                                ratio=(0.65, 1.45))]
        )

    elif augmentation == "Shadow":
        transform = A.Compose([
            transform,
            A.RandomShadow(p=0.75,
                           num_shadows_lower=1,
                           num_shadows_upper=5,
                           shadow_dimension=3)])
    elif augmentation == "Shadow2":
        transform = A.Compose([
            transform,
            A.RandomShadow(p=0.75,
                           num_shadows_lower=2,
                           num_shadows_upper=12,
                           shadow_dimension=8)])
    elif augmentation=="HorizontalFlip":
        transform = A.Compose([
            transform,
            A.HorizontalFlip(p=0.75),
        ])
    elif augmentation=="VerticalFlip":
        transform = A.Compose([
            transform,
            A.VerticalFlip(p=0.75),
        ])
    elif augmentation=="Rotate":
        transform = A.Compose([
            transform,
            A.Rotate(p=0.75),
        ])
    elif augmentation=="Transpose":
        transform = A.Compose([
            transform,
            A.Transpose(p=0.75),
        ])
    elif augmentation == "Elastic":
        transform = A.Compose([
            transform,
            A.ElasticTransform(p=0.75, alpha=120, sigma=550 * 0.05, alpha_affine=120 * 0.03),
        ])
    elif augmentation == "Elastic2":
        transform = A.Compose([
            transform,
            A.ElasticTransform(p=0.75, alpha=120, sigma=350 * 0.05, alpha_affine=120 * 0.03),
        ])
    elif augmentation == "Elastic3":
        transform = A.Compose([
            transform,
            A.ElasticTransform(p=0.75, alpha=120, sigma=750 * 0.05, alpha_affine=120 * 0.03),
        ])

    elif augmentation == "Optical":
        transform = A.Compose([
            transform,
            A.OpticalDistortion(distort_limit=1, shift_limit=0.05, p=0.75)
    ])
    elif augmentation == "Optical2":
        transform = A.Compose([
            transform,
            A.OpticalDistortion(distort_limit=1, shift_limit=0.2, p=0.75)
    ])
    elif augmentation == "Grid":
        transform = A.Compose([
            transform,
            A.GridDistortion(p=0.75,distort_limit=0.5)

    ])
    elif augmentation == "Grid2":
        transform = A.Compose([
            transform,
            A.GridDistortion(p=0.75,distort_limit=1.5)

    ])
    elif augmentation=="RGBShift":
        transform = A.Compose([
            transform,
            A.RGBShift(r_shift_limit=25, g_shift_limit=25, b_shift_limit=25, p=0.75),
        ])
    elif augmentation=="BrightnessContrast":
        transform = A.Compose([
            transform,
            A.RandomBrightnessContrast(brightness_limit=0.3, contrast_limit=0.3, p=0.75),
        ])
    elif augmentation=="Gamma":
        transform = A.Compose([
            transform,
            A.RandomGamma(gamma_limit=(50, 500), p=0.75),
        ])
    elif augmentation=="ColorJitter":
        transform = A.Compose([
            transform,
            A.ColorJitter(p=0.75),
        ])
    elif augmentation=="FancyPCA":
        transform = A.Compose([
            transform,
            A.FancyPCA(p=0.75),
        ])
    elif augmentation=="Emboss":
        transform = A.Compose([
            transform,
            A.IAAEmboss(p=0.75),
        ])
    elif augmentation=="GaussNoise":
        transform = A.Compose([
            transform,
            A.GaussNoise(p=0.75),
        ])
    elif augmentation=="GaussBlur":
        transform = A.Compose([
            transform,
            A.GaussianBlur(p=0.75,blur_limit=3),
        ])

    elif augmentation =="Morphology":
        transform = A.Compose([
            transform,
            A.Lambda(image=None,mask=custom_aug_mask,p=0.75),
        ])

    elif augmentation == "Box":
        transform = A.Compose([
            transform,
            A.CoarseDropout(max_holes=8,
                                    max_height=50,
                                    max_width=50,
                                    min_holes=5,
                                    min_height=10,
                                    min_width=10,
                                    fill_value=0,
                                    p=0.75),
        ])
    elif augmentation == "Box2":
        transform = A.Compose([
            transform,
            A.CoarseDropout(max_holes=8,
                                    max_height=50,
                                    max_width=50,
                                    min_holes=5,
                                    min_height=10,
                                    min_width=10,
                                    fill_value=0,
                                    p=0.75),
        ])

    elif augmentation == "Pixel":
        transform = A.Compose([
            transform,
            A.IAAEmboss(p=0.25),
            A.GaussianBlur(p=0.25, blur_limit=3),
            A.GaussNoise(p=0.25),
    ])
    elif augmentation == "Histo":
        transform = A.Compose([
            transform,
            A.RGBShift(r_shift_limit=25, g_shift_limit=25, b_shift_limit=25, p=0.75),
            A.RandomBrightnessContrast(brightness_limit=0.3, contrast_limit=0.3, p=0.75),
            A.RandomGamma(p=0.75,gamma_limit=(50, 500)),
            A.ColorJitter(p=0.75),
            A.FancyPCA(p=0.75)
    ])
    elif augmentation == "Dehidral":
        transform = A.Compose([
            transform,
            A.HorizontalFlip(p=0.75),
            A.VerticalFlip(p=0.75),
            A.RandomRotate90(p=0.75),
            A.Transpose(p=0.75),
        ])
    elif augmentation=="Combine0":
        transform = A.Compose([
            transform,
            A.OneOf([
                    A.RandomRotate90(p=1),
                    A.RandomResizedCrop(img_size, img_size,
                                        p = 1,
                                        scale = (0.5, 1.2),
                                        ratio = (0.85, 1.15)
                                        ),
                    A.GridDistortion(p = 1, distort_limit = 0.5),
                    A.ElasticTransform(p=1, alpha=120, sigma=750 * 0.05, alpha_affine=120 * 0.03),
                    A.RandomShadow(p = 1,
                                   num_shadows_lower = 1,
                                   num_shadows_upper = 5,
                                   shadow_dimension = 3
                                   ),
                    A.CoarseDropout(max_holes = 8,
                                    max_height = 50,
                                    max_width = 50,
                                    min_holes = 5,
                                    min_height = 10,
                                    min_width = 10,
                                    fill_value = 0,
                                    p = 1
                                    ),

                    A.HorizontalFlip(p = 1),
                    A.VerticalFlip(p = 1),
                    A.Transpose(p = 1),
                    A.RGBShift(r_shift_limit = 25, g_shift_limit = 25, b_shift_limit = 25, p = 1),
                    A.RandomBrightnessContrast(brightness_limit = 0.3, contrast_limit = 0.3, p = 1),
                    A.RandomGamma(p = 1, gamma_limit = (50, 500)),
                    A.ColorJitter(p = 1),

                    ],0.95)
                ])

    elif augmentation=="Combine1":
        transform = A.Compose([
            transform,
            A.OneOf([
                    A.RandomRotate90(p=1),
                    A.RandomResizedCrop(img_size, img_size,
                                        p = 1,
                                        scale = (0.5, 1.2),
                                        ratio = (0.85, 1.15)
                                        ),
                    A.GridDistortion(p = 1, distort_limit = 0.5),
                    A.ElasticTransform(p=1, alpha=120, sigma=750 * 0.05, alpha_affine=120 * 0.03),
                    A.RandomShadow(p = 1,
                                   num_shadows_lower = 1,
                                   num_shadows_upper = 5,
                                   shadow_dimension = 3
                                   ),
                    A.CoarseDropout(max_holes = 8,
                                    max_height = 50,
                                    max_width = 50,
                                    min_holes = 5,
                                    min_height = 10,
                                    min_width = 10,
                                    fill_value = 0,
                                    p = 1
                                    ),

                    A.HorizontalFlip(p = 1),
                    A.VerticalFlip(p = 1),
                    A.Transpose(p = 1),
                    ],0.95)
                ])
    elif augmentation=="Combine2":
        transform = A.Compose([
            transform,
            A.OneOf([
                    A.RandomRotate90(p=1),
                    A.RandomResizedCrop(img_size, img_size,
                                        p = 1,
                                        scale = (0.5, 1.2),
                                        ratio = (0.85, 1.15)
                                        ),
                    A.GridDistortion(p = 1, distort_limit = 0.5),
                    A.ElasticTransform(p=1, alpha=120, sigma=750 * 0.05, alpha_affine=120 * 0.03),
                    A.RandomShadow(p = 1,
                                   num_shadows_lower = 1,
                                   num_shadows_upper = 5,
                                   shadow_dimension = 3
                                   ),
                    A.CoarseDropout(max_holes = 8,
                                    max_height = 50,
                                    max_width = 50,
                                    min_holes = 5,
                                    min_height = 10,
                                    min_width = 10,
                                    fill_value = 0,
                                    p = 1
                                    ),

                    A.HorizontalFlip(p = 1),
                    A.VerticalFlip(p = 1),
                    A.Transpose(p = 1),
                    A.RGBShift(r_shift_limit = 25, g_shift_limit = 25, b_shift_limit = 25, p = 1),
                    A.RandomBrightnessContrast(brightness_limit = 0.3, contrast_limit = 0.3, p = 1),
                    A.RandomGamma(p = 1, gamma_limit = (50, 500)),
                    A.ColorJitter(p = 1),

                    ],0.5)
                ])
    elif augmentation=="Combine0":
        transform = A.Compose([
            transform,
            A.OneOf([
                    A.RandomRotate90(p=1),
                    A.RandomResizedCrop(img_size, img_size,
                                        p = 1,
                                        scale = (0.5, 1.2),
                                        ratio = (0.85, 1.15)
                                        ),
                    A.GridDistortion(p = 1, distort_limit = 0.5),
                    A.ElasticTransform(p=1, alpha=120, sigma=750 * 0.05, alpha_affine=120 * 0.03),
                    A.RandomShadow(p = 1,
                                   num_shadows_lower = 1,
                                   num_shadows_upper = 5,
                                   shadow_dimension = 3
                                   ),
                    A.CoarseDropout(max_holes = 8,
                                    max_height = 50,
                                    max_width = 50,
                                    min_holes = 5,
                                    min_height = 10,
                                    min_width = 10,
                                    fill_value = 0,
                                    p = 1
                                    ),

                    A.HorizontalFlip(p = 1),
                    A.VerticalFlip(p = 1),
                    A.Transpose(p = 1),
                    A.RGBShift(r_shift_limit = 25, g_shift_limit = 25, b_shift_limit = 25, p = 1),
                    A.RandomBrightnessContrast(brightness_limit = 0.3, contrast_limit = 0.3, p = 1),
                    A.RandomGamma(p = 1, gamma_limit = (50, 500)),
                    A.ColorJitter(p = 1),

                    ],1)
                ])
    elif augmentation=="Combine3":
        transform = A.Compose([
            transform,
            A.OneOf([
                    A.RandomRotate90(p=1),
                    A.RandomResizedCrop(img_size, img_size,
                                        p = 1,
                                        scale = (0.5, 1.2),
                                        ratio = (0.85, 1.15)
                                        ),
                    A.GridDistortion(p = 1, distort_limit = 0.5),
                    A.ElasticTransform(p=1, alpha=120, sigma=750 * 0.05, alpha_affine=120 * 0.03),
                    A.RandomShadow(p = 1,
                                   num_shadows_lower = 1,
                                   num_shadows_upper = 5,
                                   shadow_dimension = 3
                                   ),
                    A.CoarseDropout(max_holes = 8,
                                    max_height = 50,
                                    max_width = 50,
                                    min_holes = 5,
                                    min_height = 10,
                                    min_width = 10,
                                    fill_value = 0,
                                    p = 1
                                    ),

                    A.HorizontalFlip(p = 1),
                    A.VerticalFlip(p = 1),
                    A.Transpose(p = 1),
                    A.RGBShift(r_shift_limit = 25, g_shift_limit = 25, b_shift_limit = 25, p = 1),
                    A.RandomBrightnessContrast(brightness_limit = 0.3, contrast_limit = 0.3, p = 1),
                    A.RandomGamma(p = 1, gamma_limit = (50, 500)),
                    A.ColorJitter(p = 1),

                    ],0.75)
                ])

    elif augmentation=="Combine4":
        transform = A.Compose([
            transform,
            A.OneOf([
                    A.RandomRotate90(p=1),
                    A.HorizontalFlip(p = 1),
                    A.VerticalFlip(p = 1),
                    A.Transpose(p = 1)
            ],1),
            A.OneOf([
                    A.RandomResizedCrop(img_size, img_size,
                                        p = 1,
                                        scale = (0.5, 1.2),
                                        ratio = (0.85, 1.15)
                                        ),
                    A.GridDistortion(p = 1, distort_limit = 0.5),
                    A.ElasticTransform(p=1, alpha=120, sigma=750 * 0.05, alpha_affine=120 * 0.03),
            ],1),
            A.OneOf([
                    A.RandomShadow(p = 1,
                                   num_shadows_lower = 1,
                                   num_shadows_upper = 5,
                                   shadow_dimension = 3
                                   ),
                    A.CoarseDropout(max_holes = 8,
                                    max_height = 50,
                                    max_width = 50,
                                    min_holes = 5,
                                    min_height = 10,
                                    min_width = 10,
                                    fill_value = 0,
                                    p = 1
                                    ),
            ],1),

            A.OneOf([

                        A.RGBShift(r_shift_limit = 25, g_shift_limit = 25, b_shift_limit = 25, p = 1),
                        A.RandomBrightnessContrast(brightness_limit = 0.3, contrast_limit = 0.3, p = 1),
                        A.RandomGamma(p = 1, gamma_limit = (50, 500)),
                        A.ColorJitter(p = 1),
            ],1)
    ])
    elif augmentation == "Combine5":
        transform = A.Compose([
                transform,
                A.OneOf([
                        A.RandomRotate90(p = 1),
                        A.HorizontalFlip(p = 1),
                        A.VerticalFlip(p = 1),
                        A.Transpose(p = 1),
                        A.RandomResizedCrop(img_size, img_size,
                                            p = 1,
                                            scale = (0.5, 1.2),
                                            ratio = (0.85, 1.15)
                                            ),
                        A.GridDistortion(p = 1, distort_limit = 0.5),
                        A.ElasticTransform(p = 1, alpha = 120, sigma = 750 * 0.05, alpha_affine = 120 * 0.03),
                ], 0.5),
                A.OneOf([
                        A.RandomShadow(p = 1,
                                       num_shadows_lower = 1,
                                       num_shadows_upper = 5,
                                       shadow_dimension = 3
                                       ),
                        A.CoarseDropout(max_holes = 8,
                                        max_height = 50,
                                        max_width = 50,
                                        min_holes = 5,
                                        min_height = 10,
                                        min_width = 10,
                                        fill_value = 0,
                                        p = 1
                                        ),

                        A.RGBShift(r_shift_limit = 25, g_shift_limit = 25, b_shift_limit = 25, p = 1),
                        A.RandomBrightnessContrast(brightness_limit = 0.3, contrast_limit = 0.3, p = 1),
                        A.RandomGamma(p = 1, gamma_limit = (50, 500)),
                        A.ColorJitter(p = 1),
                ], 0.5)
        ]
        )


    elif augmentation == "Combine6":
        transform = A.Compose([
                transform,
                A.OneOf([
                        A.RandomRotate90(p = 1),
                        A.HorizontalFlip(p = 1),
                        A.VerticalFlip(p = 1),
                        A.Transpose(p = 1)
                ], 0.25),
                A.OneOf([
                        A.RandomResizedCrop(img_size, img_size,
                                            p = 1,
                                            scale = (0.5, 1.2),
                                            ratio = (0.85, 1.15)
                                            ),
                        A.GridDistortion(p = 1, distort_limit = 0.5),
                        A.ElasticTransform(p = 1, alpha = 120, sigma = 750 * 0.05, alpha_affine = 120 * 0.03),
                ], 0.25),
                A.OneOf([
                        A.RandomShadow(p = 1,
                                       num_shadows_lower = 1,
                                       num_shadows_upper = 5,
                                       shadow_dimension = 3
                                       ),
                        A.CoarseDropout(max_holes = 8,
                                        max_height = 50,
                                        max_width = 50,
                                        min_holes = 5,
                                        min_height = 10,
                                        min_width = 10,
                                        fill_value = 0,
                                        p = 1
                                        ),
                ], 0.25),

                A.OneOf([

                        A.RGBShift(r_shift_limit = 25, g_shift_limit = 25, b_shift_limit = 25, p = 1),
                        A.RandomBrightnessContrast(brightness_limit = 0.3, contrast_limit = 0.3, p = 1),
                        A.RandomGamma(p = 1, gamma_limit = (50, 500)),
                        A.ColorJitter(p = 1),
                ], 0.25)
        ]
        )
    elif augmentation == "Combine7":
        transform = A.Compose([
                transform,
                A.OneOf([
                        A.RandomRotate90(p = 1),
                        A.HorizontalFlip(p = 1),
                        A.VerticalFlip(p = 1),
                        A.Transpose(p = 1)
                ], 0.75),
                A.OneOf([
                        A.RandomResizedCrop(img_size, img_size,
                                            p = 1,
                                            scale = (0.5, 1.2),
                                            ratio = (0.85, 1.15)
                                            ),
                        A.GridDistortion(p = 1, distort_limit = 0.5),
                        A.ElasticTransform(p = 1, alpha = 120, sigma = 750 * 0.05, alpha_affine = 120 * 0.03),
                ], 0.75),
                A.OneOf([
                        A.RandomShadow(p = 1,
                                       num_shadows_lower = 1,
                                       num_shadows_upper = 5,
                                       shadow_dimension = 3
                                       ),
                        A.CoarseDropout(max_holes = 8,
                                        max_height = 50,
                                        max_width = 50,
                                        min_holes = 5,
                                        min_height = 10,
                                        min_width = 10,
                                        fill_value = 0,
                                        p = 1
                                        ),
                ], 0.75),

                A.OneOf([

                        A.RGBShift(r_shift_limit = 25, g_shift_limit = 25, b_shift_limit = 25, p = 1),
                        A.RandomBrightnessContrast(brightness_limit = 0.3, contrast_limit = 0.3, p = 1),
                        A.RandomGamma(p = 1, gamma_limit = (50, 500)),
                        A.ColorJitter(p = 1),
                ], 0.75)
        ]
        )
    # elif augmentation=="Combine1":
    #     transform = A.Compose([
    #         transform,
    #         A.OneOf([
    #                 A.HorizontalFlip(p=0.75),
    #                 A.VerticalFlip(p=0.75),
    #                 A.RandomRotate90(p=0.75),
    #                 A.Transpose(p=0.75),
    #             ],p=0.75),
    #
    #             A.OpticalDistortion(distort_limit=1, shift_limit=0.05, p=0.75),
    #             A.RandomResizedCrop(img_size, img_size,
    #                                 p=0.75,
    #                                 scale=(0.6, 1.0),
    #                                 ratio=(0.85, 1.15)),
    #         ])
    #
    # elif augmentation=="Combine2":
    #     transform = A.Compose([
    #         transform,
    #         A.OneOf([
    #             A.RandomResizedCrop(img_size, img_size,
    #                                 p=0.75,
    #                                 scale=(0.6, 1.0),
    #                                 ratio=(0.85, 1.15)),
    #             A.HorizontalFlip(p=0.75),
    #             A.VerticalFlip(p=0.75),
    #         ],p=0.75),
    #         A.OneOf([
    #             A.Lambda(image=None, mask=custom_aug_mask,p=0.75),
    #             A.RandomRotate90(p=0.75),
    #             A.Transpose(p=0.75),
    #         ]),
    #
    #         A.OneOf([
    #             A.OpticalDistortion(distort_limit=1, shift_limit=0.05, p=0.75),
    #             A.ElasticTransform(p=0.75, alpha=120, sigma=550 * 0.05, alpha_affine=120 * 0.03),
    #         ],p=0.5),
    #         A.OneOf([
    #             A.CoarseDropout(max_holes=8,
    #                             max_height=50,
    #                             max_width=50,
    #                             min_holes=5,
    #                             min_height=10,
    #                             min_width=10,
    #                             fill_value=0,
    #                             p=0.75),
    #             A.RandomShadow(p=0.75,
    #                            num_shadows_lower=3,
    #                            num_shadows_upper=5,
    #                            shadow_dimension=3)
    #         ],p=0.75),
    #         A.OneOf([
    #             A.RGBShift(r_shift_limit=25, g_shift_limit=25, b_shift_limit=25, p=0.75),
    #             A.RandomBrightnessContrast(brightness_limit=0.3, contrast_limit=0.3, p=0.75),
    #             A.RandomGamma(p=0.75, gamma_limit=(50, 500)),
    #             A.ColorJitter(p=0.75),
    #             A.FancyPCA(p=0.75)
    #         ])
    #     ])
    # elif augmentation == "Combine3":
    #     transform = A.Compose([
    #         transform,
    #         A.OneOf([
    #             A.Compose([
    #                 A.Rotate(p=0.75),
    #                 A.GridDistortion(p=0.75,distort_limit=0.5),
    #             ], p=0.75),
    #         ]),
    #         A.OneOf([
    #             A.CoarseDropout(max_holes=8,
    #                         max_height=50,
    #                         max_width=50,
    #                         min_holes=5,
    #                         min_height=10,
    #                         min_width=10,
    #                         fill_value=0,
    #                         p=0.75),
    #             A.RandomResizedCrop(img_size, img_size,
    #                             p=0.75,
    #                             scale=(0.6, 1.0),
    #                             ratio=(0.85, 1.15)),
    #         ], p=0.75)
    #     ])
    # elif augmentation=="Combine4":
    #     transform = A.Compose([
    #         transform,
    #         A.Rotate(p=0.5),
    #         A.RandomResizedCrop(img_size, img_size,
    #                             p=0.5,
    #                             scale=(0.7, 1.0),
    #                             ratio=(0.85, 1.15)),
    #
    #         A.OneOf([
    #             A.OpticalDistortion(distort_limit=1, shift_limit=0.05, p=0.75),
    #             A.ElasticTransform(p=0.75, alpha=120, sigma=550 * 0.05, alpha_affine=120 * 0.03),
    #             A.GridDistortion(p=0.75, distort_limit=0.5),
    #             A.Lambda(image=None, mask=custom_aug_mask, p=0.5),
    #         ],p=0.25),
    #         A.CoarseDropout(max_holes=8,
    #                         max_height=50,
    #                         max_width=50,
    #                         min_holes=5,
    #                         min_height=10,
    #                         min_width=10,
    #                         fill_value=0,
    #                         p=0.5),
    #         A.RandomShadow(p=0.5,
    #                        num_shadows_lower=3,
    #                        num_shadows_upper=5,
    #                        shadow_dimension=25),
    #         A.RGBShift(r_shift_limit=25, g_shift_limit=25, b_shift_limit=25, p=0.1),
    #         A.RandomBrightnessContrast(brightness_limit=0.3, contrast_limit=0.3, p=0.1),
    #         A.RandomGamma(p=0.1, gamma_limit=(50, 500)),
    #         A.ColorJitter(p=0.1),
    #         A.FancyPCA(p=0.1)
    #     ])
    #
    # elif augmentation=="Combine5":
    #     transform = A.Compose([
    #         transform,
    #         A.Rotate(p=0.5),
    #         A.OpticalDistortion(distort_limit=1, shift_limit=0.05, p=0.75),
    #         A.RandomResizedCrop(img_size, img_size,
    #                             p=0.75,
    #                             scale=(0.6, 1.0),
    #                             ratio=(0.85, 1.15)),
    #         A.CoarseDropout(max_holes=20,
    #                         max_height=50,
    #                         max_width=50,
    #                         min_holes=5,
    #                         min_height=10,
    #                         min_width=10,
    #                         fill_value=0,
    #                         p=0.75),
    #         A.RandomShadow(p=0.75,
    #                        num_shadows_lower=12,
    #                        num_shadows_upper=20,
    #                        shadow_dimension=9),
    #         A.ElasticTransform(p=0.5, alpha=120, sigma=550 * 0.05, alpha_affine=120 * 0.03),
    #         A.RGBShift(r_shift_limit=25, g_shift_limit=25, b_shift_limit=25, p=0.1),
    #         A.RandomBrightnessContrast(brightness_limit=0.3, contrast_limit=0.3, p=0.2),
    #         A.RandomGamma(p=0.2, gamma_limit=(50, 500)),
    #         A.ColorJitter(p=0.4),
    #         A.FancyPCA(p=0.2),
    #         A.GaussianBlur(p=0.1),
    #         A.GaussNoise(p=0.2),
    #         A.IAASharpen(p=0.1),
    #         A.MedianBlur(p=0.1),
    #         A.Lambda(image=None, mask=custom_aug_mask, p=0.5),
    #
    #     ])
    # elif augmentation == "Combine6":
    #     transform = A.Compose([
    #         transform,
    #         A.OneOf([
    #             A.RandomRotate90(p=0.75),
    #             A.RandomResizedCrop(img_size, img_size,
    #                                 p = 0.5,
    #                                 scale = (0.5, 1.2),
    #                                 ratio = (0.85, 1.15)
    #                                 ),
    #         ],p=0.5),
    #         A.OneOf([
    #             A.GridDistortion(p = 0.25, distort_limit = 0.5),
    #             A.ElasticTransform(p=0.25, alpha=120, sigma=750 * 0.05, alpha_affine=120 * 0.03),
    #         ],p=0.5),
    #         A.OneOf([
    #             A.RandomShadow(p = 0.75,
    #                            num_shadows_lower = 1,
    #                            num_shadows_upper = 5,
    #                            shadow_dimension = 3
    #                            ),
    #             A.CoarseDropout(max_holes = 8,
    #                             max_height = 50,
    #                             max_width = 50,
    #                             min_holes = 5,
    #                             min_height = 10,
    #                             min_width = 10,
    #                             fill_value = 0,
    #                             p = 0.75
    #                             )
    #         ],p=0.5)
    #         ])



    # transform = A.Compose([
    #     transform,
    #     # A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
    #     # ToTensorV2()
    #     ])
    if augmentation=="Autoalbument":
        transform = A.load("C:/Users/guthr/Projects/cil-road_segmentation/experiments/autoalbument-search-road/policy/latest.json")
    elif augmentation=="Autoalbument1":
        transform = A.load("C:/Users/guthr/Projects/cil-road_segmentation/experiments/autoalbument-search-road/policy/epoch_58.json")
    elif augmentation=="Autoalbument2":
        transform = A.load("C:/Users/guthr/Projects/cil-road_segmentation/experiments/autoalbument-search-road/policy/epoch_76.json")
    return transform
