#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : Daniel Guthruf
# Created Date: 27.06.2022
# =============================================================================
"""
Filename:           inference.py
Description:        Inference of test dataset
Main Libs:          segmentation_models_pytorch
Long Description:   Wandb run path needed with model_full.pt and config.default_train_config
                    Load test_dataset, model_full.pt and config.default_train_config, make
                    predictions, patch acording to make_submission.py file
                    save dummy.csv file
State:              dev.
"""
# =============================================================================
# Imports
# =============================================================================
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
import torch.backends.cudnn as cudnn
cudnn.benchmark = True

import os
import re
import yaml
import PIL
import torch
import os, glob
import argparse
import numpy as np
import albumentations as A
import segmentation_models_pytorch as smp
from pathlib import Path
from PIL import Image
from torch.utils.data import DataLoader
from src.utils.augmentations import get_augmentation
from src.utils.data_modules import RoadDataTestSet,get_preprocessing
import dotenv

#Which GPU to use
device = torch.device('cuda:1') if torch.cuda.is_available() else torch.device('cpu')


def patch_to_label(patch):
    patch = patch.astype(np.float64) / 255
    df = np.mean(patch)
    if df > foreground_threshold:
        return 1
    else:
        return 0

def mask_to_submission_strings(image_filename,im_arr, mask_dir=None,full_mask_dir=None):
    """Reads a single image and outputs the strings that should go into the submission file"""
    img_number = int(re.search(r"\d+", image_filename).group(0))
    im_arr = (im_arr.reshape(400,400)*255.0).astype(np.uint8)
    patch_size = 16
    mask = np.zeros_like(im_arr)
    for j in range(0, im_arr.shape[1], patch_size):
        for i in range(0, im_arr.shape[0], patch_size):
            patch = im_arr[i:i + patch_size, j:j + patch_size]
            label = patch_to_label(patch)
            mask[i:i+patch_size, j:j+patch_size] = int(label*255)
            yield("{:03d}_{}_{},{}".format(img_number, j, i, label))

    if mask_dir:
        save_mask_as_img(mask, os.path.join(mask_dir, "mask_" + image_filename))
    if full_mask_dir:
        save_mask_as_img(im_arr, os.path.join(full_mask_dir, "mask_" + image_filename))

def save_mask_as_img(img_arr, mask_filename):
    """
    This function takes an image array and a filename for a mask.
    It creates a new image, saves it to the file, and sets the filename to the masked version of the original file.
    """
    img = PIL.Image.fromarray(img_arr)
    os.makedirs(os.path.dirname(mask_filename), exist_ok=True)
    img.save(mask_filename)


def masks_to_submission(submission_filename, image_filenames,predictions,full_mask_dir=None, mask_dir=None):
    """Converts images into a submission file"""
    with open(submission_filename, 'w') as f:
        f.write('id,prediction\n')
        for fn,pred in zip(image_filenames,predictions):
            f.writelines('{}\n'.format(s) for s in mask_to_submission_strings(image_filename=fn,im_arr=pred, mask_dir=mask_dir,full_mask_dir=full_mask_dir))




def main():
    # Load YAML data from the file
    with open(os.path.join(RUN_PATH,'config.yaml')) as fh:
        config = yaml.load(fh, Loader=yaml.FullLoader)

    #We need to preprocess the data the same way as our training data was
    if config["encoder_weights"]["value"]!= 'None':
        preprocessing_fn = smp.encoders.get_preprocessing_fn(config["encoder"]["value"], config["encoder_weights"]["value"])
        preprocessing_fn = get_preprocessing(preprocessing_fn)
    else:
        preprocessing_fn=None
    #Load best model
    model = torch.load(os.path.join(RUN_PATH,"model_full.pt"))
    model.to(device=device)
    model.eval()

    #Get test dataset
    test_dataset = RoadDataTestSet(DATA_DIR,
                                transform=get_augmentation(augmentation="Validation",img_size=config["img_size"]["value"]),
                                seed=42,
                                with_size=False,
                                preprocess=preprocessing_fn,
                                )
    test_loader = DataLoader(
        test_dataset, batch_size=10, shuffle=False, num_workers=0, pin_memory=True,
    )

    images_list = []
    predictions_list = []

    #Do predictions on batches of test dataset and store in list
    with torch.no_grad():
        for images in test_loader:
            images = images.to(device)
            probabilities = model.predict(images)
            # We need masks and images on cpu and numpy such that we can apply crop and save it to folders
            predicted_masks = torch.sigmoid(probabilities).permute(0, 2, 3, 1).cpu().numpy() #permutes similar to reshape, such that [batch_size,height,width,channels]
            if args.threshold:
                predicted_masks = (predicted_masks > args.threshold).astype(np.uint8)
            else:
                predicted_masks = (predicted_masks > config["threshold"]["value"]).astype(np.uint8)
            images = images.permute(0, 2, 3, 1).cpu().numpy() #permutes similar to reshape, such that [batch_size,height,width,channels]
            #We crop the images because Unet needed 416,416 we need back 400 400
            crop = A.Compose([
                A.CenterCrop(height=400, width=400),
            ])

            for image,mask in zip(images,predicted_masks):
                cropped = crop(image=image, mask=mask)
                image = cropped["image"]
                mask = cropped["mask"]
                images_list.append(image)
                predictions_list.append(mask)
    #Use basic masks_to_submission functin from kaggle files
    masks_to_submission(submission_filename=os.path.join(RUN_PATH,"dummy_submission.csv"),
                        full_mask_dir=os.path.join(RUN_PATH,"full_masks/"),
                        mask_dir=os.path.join(RUN_PATH,"patched_masks/"),
                        image_filenames=test_dataset.images_filenames,
                        predictions=predictions_list)

if __name__ == '__main__':
    PROJECT_DIR = Path(__file__).parent.parent.parent
    DATA_DIR = os.path.join(PROJECT_DIR, "data/")
    CONFIG_DIR = os.path.join(PROJECT_DIR, "config/")
    WANDB_DIR = os.path.join(PROJECT_DIR)
    dotenv_path = os.path.join(PROJECT_DIR, '.env')
    dotenv.load_dotenv(dotenv_path)


    DATA_DIR = os.path.join(PROJECT_DIR, "data/")
    foreground_threshold = 0.25  # percentage of pixels of val 255 required to assign a foreground label to a patch
    parser = argparse.ArgumentParser()

    parser.add_argument('--model', type = str,
                        default = 'latest',
                        help = "latest: evaluates latest model from wandb/outputs/wandb/\n"
                               "best: evaluates best model from models/best/\n"
                               "baseline: evaluates baseline model from models/baseline/\n")
    parser.add_argument('--model_path',type=str,default = None,help = 'Path to ./files folder with model_full.pt file')
    parser.add_argument('-th', '--threshold', type = float, default = None)

    args = parser.parse_args()
    if args.model == 'latest':
        RUN_PATH= max(glob.glob(os.path.join(os.path.join(PROJECT_DIR,'wandb/outputs/wandb/'), '*/')), key = os.path.getmtime)
    elif args.model =='best':
        RUN_PATH=os.path.join(PROJECT_DIR,'models/best/')
    elif args.model =='baseline':
        RUN_PATH=os.path.join(PROJECT_DIR,'models/UnetBaseline/')
    if args.model_path:
        RUN_PATH = args.model_path

    RUN_PATH = os.path.join(RUN_PATH,'files/')
    print(f"Your choosen Path: {RUN_PATH}")

    main()