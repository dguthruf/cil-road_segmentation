import copy
import os

import albumentations as A
import cv2
from albumentations.pytorch import ToTensorV2
from matplotlib import pyplot as plt

from src.utils.data_modules import preprocess_mask


def display_valid_images(images_filenames, data_path, path,predicted_masks=None,rows=None,save=False):
    images_directory = os.path.join(data_path, "raw/training/images")
    masks_directory = os.path.join(data_path, "raw/training/groundtruth")
    cols = 3 if predicted_masks else 2
    rows = rows if rows else len(images_filenames)
    figure, ax = plt.subplots(nrows=rows, ncols=cols, figsize=(10, 24))
    for i, image_filename in enumerate(images_filenames[0:rows]):
        image = cv2.imread(os.path.join(images_directory, image_filename))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        mask = cv2.imread(os.path.join(masks_directory, image_filename.replace(".jpg", ".png")), cv2.IMREAD_UNCHANGED,)
        mask = preprocess_mask(mask)
        image = image/255.0
        ax[i, 0].imshow(image)
        ax[i, 1].imshow(mask, interpolation="nearest")

        ax[i, 0].set_title("Image")
        ax[i, 1].set_title("Ground truth mask")

        ax[i, 0].set_axis_off()
        ax[i, 1].set_axis_off()

        if predicted_masks:
            predicted_mask = predicted_masks[i]
            ax[i, 2].imshow(predicted_mask, interpolation="nearest")
            ax[i, 2].set_title("Predicted mask")
            ax[i, 2].set_axis_off()
    plt.tight_layout()
    if save:
        plt.savefig(path+'/validation_inference.png')
    else:
        plt.show()


def display_test_images(images_filenames, images_directory, path,predicted_masks=None,rows=None,save=False):
    cols = 2
    rows = rows if rows else len(images_filenames)
    figure, ax = plt.subplots(nrows=rows, ncols=cols, figsize=(10, 24))
    for i, image_filename in enumerate(images_filenames[0:rows]):
        image = cv2.imread(os.path.join(images_directory, image_filename))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        image = image/255.0
        ax[i, 0].imshow(image)

        ax[i, 0].set_title("Image")

        ax[i, 0].set_axis_off()
        predicted_mask = predicted_masks[i]
        ax[i, 1].imshow(predicted_mask, interpolation="nearest")
        ax[i, 1].set_title("Predicted mask")
        ax[i, 1].set_axis_off()
    plt.tight_layout()
    if save:
        plt.savefig(path+'/test_inference.png')
    else:
        plt.show()



def visualize_augmentations(dataset, path, idx=0, samples=5,save=False):
    dataset = copy.deepcopy(dataset)
    dataset.transform = A.Compose([t for t in dataset.transform if not isinstance(t, (A.Normalize, ToTensorV2))])
    figure, ax = plt.subplots(nrows=samples, ncols=2, figsize=(10, 24))
    for i in range(samples):
        image, mask = dataset[idx]
        ax[i, 0].imshow(image)
        ax[i, 1].imshow(mask, interpolation="nearest")
        ax[i, 0].set_title("Augmented image")
        ax[i, 1].set_title("Augmented mask")
        ax[i, 0].set_axis_off()
        ax[i, 1].set_axis_off()
    plt.tight_layout()
    if save:
        plt.savefig(path+'/augmentations.png')
    else:
        plt.show()
